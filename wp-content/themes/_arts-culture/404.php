<?php get_header(); ?>
<div id="container" class="wrapper">
	<div id="contentwrapper"> 
		<div id="content">

						<div id="post-<?php the_ID(); ?>" <?php post_class('entry'); ?>>
							<h2 class="entry_title">
								<?php _e( '404 Not Found.', 'artcltr' ); ?>
							</h2>	
							<p><?php _e('Sorry the page you were looking is not here.','artcltr'); ?></p>
						</div>
		</div><!-- #content -->
				
		<div id="sidebar">
			<?php get_sidebar(); ?>
		</div><!-- #Sidebar -->		
	</div><!-- #contentwrapper -->		
</div><!-- #Container -->
<?php get_footer(); ?>