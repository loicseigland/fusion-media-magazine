19.12.2013 Arts and Culture 1.5
------------------------------------------
 - Theme control panel - collapsed view fix
 - Theme control panel - WP 3.8 style changes
 - Theme control panel - Google Analytics code fix
	-- framework/admin folder
 - wp-caption align center
	-- styles directory

 -> Arts and Culture 1.4
------------------------------------------
 - InnerPage slider fix
	-- inc/theme-gallery.php
 - Image border size adjustment
	-- styles directory
 - Magazine style category template fix
	-- functions.php
	-- archive-magazine.php
 - Blog page template - number of posts adjustment
	-- tpl-blog.php

 -> Arts and Culture 1.3
------------------------------------------
 - Category template - Archive with slider fix
	-- styles directory
	-- loop-slider.php

 -> Arts and Culture 1.2
------------------------------------------
 - Custom Query Widget
	-- functions.php
 - Do not duplicate option
	-- inc/theme-options.php
	-- home.php

 -> Arts and Culture 1.1
------------------------------------------
 - Update innerpage slider javascript library.
	-- inc/styles directory
	-- inc/js directory
	-- inc/theme-js.php
	-- inc/script-init.php

 -> Arts and Culture 1.0
------------------------------------------
 - Add Custom Query Widget

 -> Arts and Culture 0.9
------------------------------------------
 - Remove built-in theme widgets to support Gabfire Widget Pack plugin


 -> Arts and Culture 0.8
------------------------------------------
 - Fix Archive Widget
 - Responsive layout wp-caption display fix

 -> Arts and Culture 0.7
------------------------------------------
 - jQuery Call update
 
 ->  Arts and Culture 0.6
------------------------------------------
 - Remove outdated JS files
 - Remove fancybox Script
 - Remove SuperFish Script
 - Footer link - Cover image fix
 - Twitter widget fix

-> Arts and Culture 0.5
------------------------------------------
 - JS framework update

Arts and Culture 0.4
------------------------------------------
 - Fix homepage 468px ad display - home.php / ads/home_468x60.php
 - Homepage mid column - image size fix - home.php
 - Disable featured slider via theme control panel - home.php
 - Update share on twitter - gabfire-widgets.php
 - Fancybox CSS file update
 - Gabfire Media framework file update
 - Gabfire Widgets file update

Arts and Culture 0.3
------------------------------------------
 - Remove number of posts thats displayed below bottom slider - home.php
 - Update framework - framework folder
 - Fix string for custom navigation (primary to secondary) - functions.php
 
Arts and Culture 0.2
------------------------------------------
 - Fix expand.png link
 - Implement 0 post - category block disable functionality

Arts and Culture 0.1
------------------------------------------
   - Initial release
