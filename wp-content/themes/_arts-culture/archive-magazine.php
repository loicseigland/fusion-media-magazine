<?php if(!is_paged()) { ?>
<div id="container" class="wrapper">

<div class="categoryhead">
	<h3 class="categoryname"><?php single_cat_title(); ?></h3>
	<?php echo category_description(); ?>
</div>

<div id="magcontent">
	<div class="wrapper">
	
	
	<?php 
	$count = 1;
	if (have_posts()) : while (have_posts()) : the_post();			
	$gab_thumb = get_post_meta($post->ID, 'thumbnail', true);
	$gab_video = get_post_meta($post->ID, 'video', true);
	$gab_flv = get_post_meta($post->ID, 'videoflv', true);
	$ad_flv = get_post_meta($post->ID, 'adflv', true);
	$gab_iframe = get_post_meta($post->ID, 'iframe', true);
	?>
	
		<?php if($count == 1) { ?>
			<div class="bigpost left">
				<?php
				gab_media(array(
					'name' => 'ac-mag1', 
					'imgtag' => 1,
					'link' => 1,
					'enable_video' => 1, 
					'catch_image' => of_get_option('of_catch_img', 0),
					'video_id' => 'ac-mag_upleft', 
					'enable_thumb' => 1,
					'resize_type' => 'c', 
					'media_width' => 463, 
					'media_height' => 310, 
					'thumb_align' => 'aligncenter',
					'enable_default' => 1,
					'default_name' => 'magazine-top.jpg'
				)); 
				?>
				
				<h2 class="posttitle">
					<a href="<?php the_permalink() ?>" rel="bookmark" title="<?php printf( esc_attr__( 'Permalink to %s', 'artcltr' ), the_title_attribute( 'echo=0' ) ); ?>" >
						<?php the_title(); ?>
					</a>
				</h2>
				<p><?php echo string_limit_words(get_the_excerpt(), 15); ?>&hellip;</p>
			</div>
		<?php } ?>
	
	
		<?php if($count == 2) { ?>
		<div class="bigpost right">
			<?php
			gab_media(array(
				'name' => 'ac-mag1', 
				'imgtag' => 1,
				'link' => 1,
				'enable_video' => 1, 
				'catch_image' => of_get_option('of_catch_img', 0),
				'video_id' => 'ac-mag_upright', 
				'enable_thumb' => 1,
				'resize_type' => 'c', 
				'media_width' => 463, 
				'media_height' => 310, 
				'thumb_align' => 'aligncenter',
				'enable_default' => 1,
				'default_name' => 'magazine-top.jpg'
			)); 
			?>
			
			<h2 class="posttitle">
				<a href="<?php the_permalink() ?>" rel="bookmark" title="<?php printf( esc_attr__( 'Permalink to %s', 'artcltr' ), the_title_attribute( 'echo=0' ) ); ?>" >
					<?php the_title(); ?>
				</a>
			</h2>
			<p><?php echo string_limit_words(get_the_excerpt(), 15); ?>&hellip;</p>
		</div>	
	
		<div class="clear"></div>
		
		<div class="leftcol">
			<?php if (intval(of_get_option('of_ac_custompostsnr')) > 0 ) { ?>
				<?php gab_dynamic_sidebar('Magazine-Leftsidebar'); ?>
			<?php } ?>
		</div><!-- /leftcol -->
		
		<div class="midcol">
			<div class="colwrapper"><?php } ?>
			
			<?php if ($count == 3) { ?>
	
				<div class="featuredpost lastpost">
													
					<?php
					gab_media(array(
						'name' => 'ac-mag2', 
						'imgtag' => 1,
						'link' => 1,
						'enable_video' => 1, 
						'catch_image' => of_get_option('of_catch_img', 0),
						'video_id' => 'ac-mag2', 
						'enable_thumb' => 1,
						'resize_type' => 'c', 
						'media_width' => 479, 
						'media_height' => 300, 
						'thumb_align' => 'aligncenter',
						'enable_default' => 0
					)); 
					?>
					<h2 class="posttitle">
						<a href="<?php the_permalink() ?>" rel="bookmark" title="<?php printf( esc_attr__( 'Permalink to %s', 'artcltr' ), the_title_attribute( 'echo=0' ) ); ?>" >
							<?php the_title(); ?>
						</a>
					</h2>
					<?php echo '<p>' . string_limit_words(get_the_excerpt(), 16) . '&hellip;</p>'; ?>							
				</div>
			</div><!-- colwrapper -->
			
			<div class="colwrapper border-bottom">
				<div class="col left"><?php } ?>	
				
					<?php if (($count > 3) and (7 > $count)) { ?>
						<div class="featuredpost<?php if($count == 6) { echo ' lastpost'; } ?>">
							<?php
							gab_media(array(
								'name' => 'ac-mag3', 
								'imgtag' => 1,
								'link' => 1,
								'enable_video' => 1, 
								'catch_image' => of_get_option('of_catch_img', 0),
								'enable_thumb' => 1,
								'video_id' => 'ac-mag3', 
								'resize_type' => 'c', 
								'media_width' => 227, 
								'media_height' =>  120, 
								'thumb_align' => 'aligncenter',
								'enable_default' => 0
							)); 
							?>
							
							<h2 class="posttitle s_title">
								<a href="<?php the_permalink() ?>" rel="bookmark" title="<?php printf( esc_attr__( 'Permalink to %s', 'artcltr' ), the_title_attribute( 'echo=0' ) ); ?>" >
									<?php gab_posttitle(40,'&hellip;'); ?>
								</a>
							</h2>											
							
							<?php echo '<p>' . string_limit_words(get_the_excerpt(), 11) . '&hellip;</p>'; ?>					
						</div>
					<?php } ?>
				
				<?php if ($count == 6) { ?>	
				</div>

				<div class="col right"><?php } ?>	
				
					<?php if (($count > 6) and (10 > $count)) { ?>
						<div class="featuredpost<?php if($count == 9) { echo ' lastpost'; } ?>">
							<?php
							gab_media(array(
								'name' => 'ac-mag3', 
								'imgtag' => 1,
								'link' => 1,
								'enable_video' => 1, 
								'catch_image' => of_get_option('of_catch_img', 0),
								'enable_thumb' => 1,
								'video_id' => 'ac-mag3_1', 
								'resize_type' => 'c', 
								'media_width' => 227, 
								'media_height' =>  120, 
								'thumb_align' => 'aligncenter',
								'enable_default' => 0
							)); 
							?>
							
							<h2 class="posttitle s_title">
								<a href="<?php the_permalink() ?>" rel="bookmark" title="<?php printf( esc_attr__( 'Permalink to %s', 'artcltr' ), the_title_attribute( 'echo=0' ) ); ?>" >
									<?php gab_posttitle(40,'&hellip;'); ?>
								</a>
							</h2>											
							
							<?php echo '<p>' . string_limit_words(get_the_excerpt(), 11) . '&hellip;</p>'; ?>					
						</div>
					<?php } ?>
					
				<?php if ($count == 9) { ?>	
				</div>
				<div class="clear"></div>
			</div><!-- colwrapper -->
			
			<div id="bottom-slider"><?php } ?>
				<?php if($count > 9) { ?>
				<div class="item">
					<?php
					gab_media(array(
						'name' => 'ac-mag4', 
						'imgtag' => 1,
						'link' => 1,
						'enable_video' => 1, 
						'catch_image' => of_get_option('of_catch_img', 0),
						'video_id' => 'midslider', 
						'enable_thumb' => 1,
						'resize_type' => 'c', 
						'media_width' => 479, 
						'media_height' => 265, 
						'thumb_align' => 'null',
						'enable_default' => 0
					)); 
					?>
					<div class="slidetext">
						<h2 class="posttitle">
							<a href="<?php the_permalink() ?>" rel="bookmark" title="<?php printf( esc_attr__( 'Permalink to %s', 'artcltr' ), the_title_attribute( 'echo=0' ) ); ?>" >
								<?php the_title(); ?>
							</a>
						</h2>
					</div>
				</div><!-- /item --><?php } ?>
				
			<?php if($count == 12) { ?>
			</div><!-- /bottom-slider -->
			<div id="bottom_nav"></div>

			<p class="previousentries"><?php posts_nav_link('','', __('Click here to see more entries', 'artcltr')); ?></p>
		
		</div><!-- /midcol --> <?php } ?>
	<?php $count++; endwhile; endif; ?>
		
	<div class="rightcol">
		<?php gab_dynamic_sidebar('Magazine-Rightsidebar'); ?>
	</div><!-- /rightcol -->
	
	<div class="clear"></div>
	
	</div><!-- /wrapper -->
</div><!-- /magcontent -->

</div><!-- #Container -->
<?php
} else {
	include (TEMPLATEPATH . '/archive-default.php'); 
} ?>