<?php if(!is_paged()) { ?>
	<div id="container" class="wrapper">

			<div class="categoryhead">
				<h3 class="categoryname"><?php single_cat_title(); ?></h3>
				<?php echo category_description(); ?>
			</div>
			
			<?php 
			get_template_part( 'loop', 'slider' );
			
			// load pagination
			if (($wp_query->max_num_pages > 1) && (function_exists('pagination'))) {
				pagination($additional_loop->max_num_pages);
			}
			?>
			
			</div><!-- #content -->
					
			<div id="sidebar">
				<?php get_sidebar(); ?>
			</div><!-- #Sidebar -->		
		</div><!-- #contentwrapper -->
	</div><!-- #Container -->
<?php } else {
	include (TEMPLATEPATH . '/archive-default.php'); 
}