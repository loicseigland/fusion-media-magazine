<?php 
/* Check if this is a photo/video custom post type archive
 * or a category page defined on theme options page
 * to display media layout; If true load archive-media.php.
 *
 * If this is a category archive, which is defined on theme
 * option page to display in 2 column format, load archive-2col.php
 *
 * If both conditions above are false, get archive-default.php
 */ 
 
/*
 // Add css class 'last' to last posts in archive query [ used in conjunction with post_class('archive-post') ]
add_filter('post_class', 'last_post_class');
function last_post_class($classes) {
    // Regular Archives
  global $wp_query;
  if(($wp_query->current_post+1) == $wp_query->post_count)
        $classes[] = 'last';

    // Media Archive
    global $media_query;
  if(($media_query->current_post+1) == $media_query->post_count)
        $classes[] = 'last';

    // Events Archive
    global $events_query;
  if(($events_query->current_post+1) == $events_query->post_count)
        $classes[] = 'last';

    // Author Archive
    global $author_query;
  if(($author_query->current_post+1) == $author_query->post_count)
        $classes[] = 'last';

  return $classes;
}
*/
 
get_header(); 

	if(of_get_option('of_ac_cslide') <> '' && is_category(explode(',', of_get_option('of_ac_cslide')))) {
		include (TEMPLATEPATH . '/archive-slider.php'); 
	}
	elseif(of_get_option('of_ac_mag') <> '' && is_category(explode(',', of_get_option('of_ac_mag')))) {
		include (TEMPLATEPATH . '/archive-magazine.php'); 
	}
	elseif( is_tax('gallery-cat') or is_post_type_archive( 'gab_gallery' ) or (of_get_option('of_ac_mediatmp') <> '' && is_category(explode(',',of_get_option('of_ac_mediatmp'))))){	
		include (TEMPLATEPATH . '/archive-media.php'); 	
	} 
	elseif(of_get_option('of_ac_2col') <> '' && is_category(explode(',', of_get_option('of_ac_2col')))) {
		include (TEMPLATEPATH . '/archive-2col.php'); 
	}
	elseif(of_get_option('of_ac_4col') <> '' && is_category(explode(',', of_get_option('of_ac_4col')))) {
		include (TEMPLATEPATH . '/archive-4col.php'); 
	}
	else {
		include (TEMPLATEPATH . '/archive-default.php'); 
	}
	
get_footer(); 