<?php get_header(); ?>

<div id="container" class="wrapper">

	<div id="contentwrapper"> 
		<div id="content">
	
			<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
			<div id="post-<?php the_ID(); ?>" <?php post_class('entry gab_attachmentpage'); ?>>
			
				<h1 class="entry_title">
					<?php
					$parent_title = get_the_title($post->post_parent);
					echo $parent_title;
					?>
				</h1>
						
				<!-- Post Meta -->
				<p class="top_postmeta">
					<span class="entrydate metaitem"><?php _e('Posted on','artcltr'); echo ' ' . get_the_date(); ?></span>
					<span class="entryby metaitem last"><a href="<?php echo get_author_posts_url(get_the_author_meta( 'ID' )); ?>"><?php _e('By','artcltr'); echo ' ' . get_the_author(); ?></a></span>
				</p>

				<div id="attachment-nav">
					<div class="attachment-nav">
						<?php previous_image_link( false, __('&laquo; Previous Image','artcltr')); ?> <a href="<?php echo get_permalink($post->post_parent); ?>"><?php _e('Back to Post','artcltr'); ?></a> <?php next_image_link( false, __('Next Image &raquo;','artcltr')); ?>
					</div>
				</div>
				
				<?php
				if (of_get_option('of_wpmumode')==0) {
					if (wp_attachment_is_image($post->id)) {
						$att_image = wp_get_attachment_image_src( $post->id, "ac-loop-default");
					}
					if(is_multisite()) { ?>
						<img src="<?php echo get_template_directory_uri(); ?>/timthumb.php?src=<?php echo redirect_wpmu($att_image[0]); ?>&amp;q=90&amp;w=630&amp;zc=1" class="attachment-full aligncenter" alt="" />
					<?php } else { ?>
						<img src="<?php echo get_template_directory_uri(); ?>/timthumb.php?src=<?php echo $att_image[0]; ?>&amp;q=90&amp;w=630&amp;zc=1" class="attachment-full aligncenter" alt="" />
					<?php }
				} else {
					if (wp_attachment_is_image($post->id)) {
						$att_image = wp_get_attachment_image_src( $post->id, "ac-loop-default");
					} ?>
					<img src="<?php echo $att_image[0]; ?>" alt="<?php the_title(); ?>" class="attachment-full aligncenter" />
				<?php } ?>
				
				<div class="clear"></div>
				
				<?php
					$count = 1;
					
					$args = array(
					'post_type' => 'attachment',
					'numberposts' => -1,
					'order' => 'ASC',
					'post_parent' => $post->post_parent);
					$attachments = get_posts($args);

					
					if ($attachments){
						foreach ($attachments as $attachment){
							echo '<div class="gab_attc_thumbwrap'; if($count % 6 == 0){ echo $lastpost = ' last'; }
							echo '"><a href="'.get_attachment_link($attachment->ID, false).'">'.wp_get_attachment_image($attachment->ID, 'thumbnail').'</a></div>';									
							$count++;
						}
					}
				?>		
			</div><!-- /.post .entry -->
			<?php endwhile; else : endif; ?>
			
			<?php comments_template(); ?>	
		</div><!-- #content -->
				
		<div id="sidebar">
			<?php get_sidebar(); ?>
		</div><!-- #Sidebar -->		
	</div><!-- #contentwrapper -->		
</div><!-- #Container -->

<?php get_footer(); ?>