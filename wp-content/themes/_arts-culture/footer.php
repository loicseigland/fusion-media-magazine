<div id="footernav" class="wrapper">
	<ul class="footernav dropdown">
		<li class="first<?php if(is_home() ) { ?> current-cat<?php } ?>"><a href="<?php echo home_url('/'); ?>" title="<?php bloginfo('description'); ?>"><?php _e('Home','artcltr'); ?></a></li>
		<?php
		if(of_get_option('of_nav3', 0) == 1) { 
			wp_nav_menu( array('theme_location' => 'footer', 'container' => false, 'items_wrap' => '%3$s'));
		} else {
			wp_list_pages('sort_column=menu_order&title_li=&exclude='. of_get_option('of_ex_pages')); 
		}
		?>
	</ul>
</div><!-- #footernav -->

<div id="footer">
	<div class="wrapper">
		<div class="footer1">
			<?php gab_dynamic_sidebar('Footer-1');
			
			if(of_get_option('of_footer1link') <> "" ) { 
				echo '<a href="' . of_get_option('of_footer1link') . '">';
				echo '<img src="'.of_get_option('of_footer1img').'" alt="" class="coverimg" />';
				echo '</a>';
			} elseif(of_get_option('of_footer1img') <> "" ) {
				echo '<img src="'.of_get_option('of_footer1img').'" alt="" class="coverimg" />';
			}
			?>
		</div>
		
		<div class="footerinner">
			<div class="footer2">
				<?php gab_dynamic_sidebar('Footer-2'); ?>
			</div>
			
			<div class="footer3">
				<?php gab_dynamic_sidebar('Footer-3'); ?>
			</div>
			
			<div class="footer4">
				<?php gab_dynamic_sidebar('Footer-4'); ?>
			</div>
			
			<div class="footer5">
				<?php gab_dynamic_sidebar('Footer-5'); ?>
			</div>
		</div>
		<div class="clear"></div>
	</div><!-- /wrapper -->
	
</div><!-- /footer -->


<div id="footer_data">	
	<div class="wrapper">

	<p id="footer-left-side">
		<?php /* Replace default text if option is set */
		if( of_get_option('of_footer1') == 1){
			echo of_get_option('of_footer1_text');
		} else { 
		?>
			&copy; <?php echo the_date('Y'); ?>, <a href="#top" title="<?php bloginfo('name'); ?>" rel="home"><strong>&uarr;</strong> <?php bloginfo('name'); ?></a>
		<?php } ?>
	</p><!-- #site-info -->
				
	<p id="footer-right-side">
		<?php /* Replace default text if option is set */
		if( of_get_option('of_footer2') == 1){ 
			echo of_get_option('of_footer2_text');
		} else {
			wp_loginout(); 
			if ( is_user_logged_in() ) { 
				echo '-'; ?>
				<a href="<?php echo home_url('/'); ?>wp-admin/edit.php">Posts</a> - 
				<a href="<?php echo home_url('/'); ?>wp-admin/post-new.php">Add New</a>
			<?php } ?> - 			
		<?php } ?>
		<a href="http://wordpress.org/" title="<?php esc_attr_e('Semantic Personal Publishing Platform', 'artcltr'); ?>" rel="generator"><?php _e('Powered by WordPress', 'artcltr'); ?></a> - 
		Designed by <a href="http://www.gabfirethemes.com/" title="Premium WordPress Themes">Gabfire Themes</a> 
		<?php wp_footer(); ?>
	</p> <!-- #footer-right-side -->
	</div><!-- /wrapper -->
</div><!-- /footer_data -->

</body>
</html>