<?php
	// DIRECTORIES
	define('OF_FILEPATH', TEMPLATEPATH);
	define('OF_DIRECTORY', get_template_directory_uri());

	define('GABFIRE_INC_PATH', TEMPLATEPATH . '/inc');
	define('GABFIRE_FRAMEWORK_PATH', TEMPLATEPATH . '/framework');
	define('GABFIRE_INC_DIR', get_template_directory_uri() . '/inc');
	define('GABFIRE_FUNCTIONS_PATH', TEMPLATEPATH . '/inc/functions');
	define('GABFIRE_JS_DIR', get_template_directory_uri() . '/inc/js');
	
	// OPTION PANEL	
	if ( !function_exists( 'optionsframework_init' ) ) {
		define( 'OPTIONS_FRAMEWORK_DIRECTORY', get_template_directory_uri() . '/framework/admin/' );
		require_once dirname( __FILE__ ) . '/framework/admin/options-framework.php';
	}	
	
	/* This builds dashboard menu */
	require_once (GABFIRE_FRAMEWORK_PATH . '/admin/admin-menu.php');

	require_once (GABFIRE_INC_PATH . '/theme-js.php'); // Load theme Javascripts
	require_once (GABFIRE_INC_PATH . '/theme-comments.php');	// Load custom comments template
	require_once (GABFIRE_INC_PATH . '/widgetize-theme.php'); // Register sidebars
	require_once (GABFIRE_INC_PATH . '/I18n-functions.php'); // localization support
	require_once (GABFIRE_INC_PATH . '/post-thumbnails.php'); // Load theme thumbnails
	require_once (GABFIRE_INC_PATH . '/script-init.php'); // Javascript init
	require_once (GABFIRE_INC_PATH . '/theme-cpt.php'); // Custom post type and taxonomies (CPT is used with CMS themes only)
	require_once (GABFIRE_INC_PATH . '/custom-fields.php'); // Breadcrumb function
	
	// FRAMEWORK FILES
	require_once (GABFIRE_FRAMEWORK_PATH . '/functions/breadcrumb.php'); // Breadcrumb function
	require_once (GABFIRE_FRAMEWORK_PATH . '/functions/misc-functions.php'); // Misc theme functions
	require_once (GABFIRE_FRAMEWORK_PATH . '/functions/dashboard-widget.php'); // Gabfire Themes RSS widget for WP Dashboard
	require_once (GABFIRE_FRAMEWORK_PATH . '/functions/gabfire-media.php'); // Gabfire Media Module

	/* Add custom navigation support
	 * For header navigations check the core files at
	 * inc/functions/misc-functions file
	 */
	// Add custom navigation support
	register_nav_menus(array(
		'primary' => 'Primary Navigation',
		'secondary' => 'Secondary Navigation',
		'footer' => 'Footer Navigation'
	));
	

 class gab_custom_widget extends WP_Widget {
 
	function gab_custom_widget() {
		$widget_ops = array( 'classname' => 'gab_custom_widget', 'description' => 'Display Custom Queries' );
		$control_ops = array( 'width' => 520, 'height' => 350, 'id_base' => 'gab_custom_widget' );
		$this->WP_Widget( 'gab_custom_widget', 'Gabfire Widget : Custom Query', $widget_ops, $control_ops);
	}
 
	function widget($args, $instance) {      
		extract( $args );
		$title    = $instance['title'];
		$swap = $instance['swap'] ? '1' : '0';
		$video = $instance['video'] ? '1' : '0';
		$c_image = $instance['c_image'];
		$c_link = $instance['c_link'];
		$postnr    = $instance['postnr'];
		$postids    = $instance['postids'];
		$cat_or_postpage    = $instance['cat_or_postpage'];
		$post_order    = $instance['post_order'];
		$d_thumb    = $instance['d_thumb'] ? '1' : '0';
		$postmeta    = $instance['postmeta'] ? '1' : '0';
		$media_w    = $instance['media_w'];
		$media_h    = $instance['media_h'];
		$excerpt_l    = $instance['excerpt_l'];
		$thumbalign    = $instance['thumbalign'];
		$postcls    = $instance['postcls'];
		$titlecls    = $instance['titlecls'];
		
		echo $before_widget;
		
			if ( $title ) {
				echo '<p class="catname">';
					if ( $c_link ) { echo '<a href="' . $c_link . '">'; }
						echo $title;
					if ( $c_link ) { echo '</a>'; }
				echo '</p>';
			}
			
			if ( $c_image ) { 
				if ( $c_link ) { echo '<a href="' . $c_link . '">'; }
					echo '<img style="display:block;margin:0 0 7px;line-height:0" src="'.$c_image.'" alt="" />'; 
				if ( $c_link ) { echo '</a>'; }
			}
			$count = 0;
			global $do_not_duplicate, $post, $page;
			
			if ( $cat_or_postpage == 0 ) { 
				$args = array(
				  'post_type' =>array('page','post'),
				  'post__in'=> explode(',', $postids),
				);
			} else {
				$args = array(
				  'post__not_in'=>$do_not_duplicate,
				  'posts_per_page' => $postnr,
				  'orderby' => $post_order,
				  'cat' => $postids
				);	
			}
			
			$gab_query = new WP_Query();$gab_query->query($args); 
			while ($gab_query->have_posts()) : $gab_query->the_post();
			if (of_get_option('of_dnd') == 1) { $do_not_duplicate[] = $post->ID; }
			?>

				<div class="<?php if ( $postcls ) { echo $postcls; } else { echo 'featuredpost'; } if($count == $postnr) { echo ' lastpost'; } ?>">

					<?php
					if ( $d_thumb ) {
						if ( $swap ) { ?>
							<h2 class="<?php if ( $titlecls ) { echo $titlecls; } else { echo 'posttitle'; } ?>">
								<a href="<?php the_permalink() ?>" rel="bookmark" title="<?php printf( esc_attr__( 'Permalink to %s', 'gabfire' ), the_title_attribute( 'echo=0' ) ); ?>" ><?php the_title(); ?></a>
							</h2>   
							<?php
							gab_media(array(
								'name' => 'gabfire',
								'imgtag' => 1,
								'link' => 1,
								'enable_video' => $video,
								'video_id' => 'custom-widget',
								'catch_image' => 0,
								'enable_thumb' => 1,
								'resize_type' => 'c',
								'media_width' => $media_w, 
								'media_height' => $media_h, 
								'thumb_align' => $thumbalign,
								'enable_default' => 0
							));							
						} else {
							gab_media(array(
								'name' => 'gabfire',
								'imgtag' => 1,
								'link' => 1,
								'enable_video' => $video,
								'video_id' => 'custom-widget',
								'catch_image' => 0,
								'enable_thumb' => 1,
								'resize_type' => 'c',
								'media_width' => $media_w, 
								'media_height' => $media_h, 
								'thumb_align' => $thumbalign,
								'enable_default' => 0
							)); ?>
							<h2 class="posttitle">
								<a href="<?php the_permalink() ?>" rel="bookmark" title="<?php printf( esc_attr__( 'Permalink to %s', 'gabfire' ), the_title_attribute( 'echo=0' ) ); ?>" ><?php the_title(); ?></a>
							</h2><?php							
						}
					} else { ?>
						<h2 class="<?php if ( $titlecls ) { echo $titlecls; } else { echo 'posttitle'; } ?>">
							<a href="<?php the_permalink() ?>" rel="bookmark" title="<?php printf( esc_attr__( 'Permalink to %s', 'gabfire' ), the_title_attribute( 'echo=0' ) ); ?>" ><?php the_title(); ?></a>
						</h2>
					<?php }
					
					if ( $excerpt_l != 0 ) {
						echo '<p>' . string_limit_words(get_the_excerpt(), $excerpt_l) . '&hellip;</p>';
					}
					
					if ( $postmeta ) {
						gab_postmeta(); 
					} ?>
					
				</div><!-- .featuredpost -->
			<?php $count++; endwhile; wp_reset_query(); ?>
	<?php         
		echo $after_widget; 
	}
	
	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['title'] = strip_tags($new_instance['title']);
		$instance['video']     = $new_instance['video'] ? '1' : '0';
		$instance['swap']     = $new_instance['swap'] ? '1' : '0';  
		$instance['postnr']     = (int)$new_instance['postnr'];
		$instance['postids']     = $new_instance['postids'];
		$instance['c_link']     = $new_instance['c_link'];
		$instance['c_image']     = $new_instance['c_image']; 
		$instance['cat_or_postpage']     = $new_instance['cat_or_postpage']; 
		$instance['post_order']     = $new_instance['post_order']; 
		$instance['d_thumb']     = $new_instance['d_thumb'] ? '1' : '0';
		$instance['postmeta']     = $new_instance['postmeta'] ? '1' : '0';
		$instance['media_w']     = (int)$new_instance['media_w']; 
		$instance['media_h']     = (int)$new_instance['media_h']; 
		$instance['excerpt_l']     = (int)$new_instance['excerpt_l'];
		$instance['thumbalign']     = $new_instance['thumbalign'];
		$instance['postcls']     = $new_instance['postcls'];
		$instance['titlecls']     = $new_instance['titlecls'];
		return $instance;
	}

	function form( $instance ) {
		$defaults = array( 'title' => 'Custom Query', 'postcls' => 'featuredpost', 'titlecls' => 'posttitle');
		$instance = wp_parse_args( (array) $instance, $defaults ); 
		?>
		
		<p style="background-color: #efefef;border:1px solid #ddd;padding:10px;">Huh, you think I am just another widget? Believe it or not, I'm quite powerful. You'll be amazed to learn that I can be used to build an entire site :) So let's start, shall we?<p>
		
		<p>
			<label for="<?php echo $this->get_field_id('title'); ?>">Section or Category Title <span style="color:#aaa">(leave empty for no heading)</span></label>
			<input class="widefat"  id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr( $instance['title'] ); ?>" />
		</p>
		
		<p>
			<label for="<?php echo $this->get_field_id('c_image'); ?>">Section or Category Image URL (replaces text title) <span style="color:#aaa">(ex: http://www.domain.com/image.jpg)</span></label> 
			<input class="widefat"  id="<?php echo $this->get_field_id('c_image'); ?>" name="<?php echo $this->get_field_name('c_image'); ?>" type="text" value="<?php echo esc_attr( $instance['c_image'] ); ?>" />
		</p>

		<p>
			<label for="<?php echo $this->get_field_id('c_link'); ?>">Link for Section or Category title <span style="color:#aaa">(Leave empty to disable)</span></label> 
			<input class="widefat"  id="<?php echo $this->get_field_id('c_link'); ?>" name="<?php echo $this->get_field_name('c_link'); ?>" type="text" value="<?php echo esc_attr( $instance['c_link'] ); ?>" />
		</p>

		<p>
			<label for="<?php echo $this->get_field_id('cat_or_postpage'); ?>">Do you want to run a query based on Category or Post/Page ID</label><br />
			<select id="<?php echo $this->get_field_id( 'cat_or_postpage' ); ?>" name="<?php echo $this->get_field_name( 'cat_or_postpage' ); ?>" style="width:480px">
				<option value="1" <?php if ( '1' == $instance['cat_or_postpage'] ) echo 'selected="selected"'; ?>>Category</option>
				<option value="0" <?php if ( '0' == $instance['cat_or_postpage'] ) echo 'selected="selected"'; ?>>Post/Page</option>    
			</select>
		</p>		
		
		<p>
			<label for="<?php echo $this->get_field_id('postids'); ?>">Enter <a href="http://www.gabfirethemes.com/how-to-check-category-ids/">Category ID(s)</a> - OR - Post/Page ID(s) <span style="color:#aaa">(ex:3,5,99 comma separated, no spaces)</span></label>
			<input class="widefat"  id="<?php echo $this->get_field_id('postids'); ?>" name="<?php echo $this->get_field_name('postids'); ?>" type="text" value="<?php echo esc_attr( $instance['postids'] ); ?>" />
		</p>   		
		
		<p>
			<label for="<?php echo $this->get_field_id('post_order'); ?>">Post Order: only if category option is selected as base for query</label><br />
			<select id="<?php echo $this->get_field_id( 'post_order' ); ?>" name="<?php echo $this->get_field_name( 'post_order' ); ?>" style="width:480px">
				<option value="date" <?php if ( 'date' == $instance['post_order'] ) echo 'selected="selected"'; ?>>Most Recent</option>
				<option value="rand" <?php if ( 'rand' == $instance['post_order'] ) echo 'selected="selected"'; ?>>Random</option>
				<option value="comment_count" <?php if ( 'comment_count' == $instance['post_order'] ) echo 'selected="selected"'; ?>>Most Popular (Criteria: Comment Count)</option>
			</select>
		</p>			
		
		<p>
			<label for="<?php echo $this->get_field_name( 'postnr' ); ?>">Number of entries to display</label>
			<select id="<?php echo $this->get_field_id( 'postnr' ); ?>" name="<?php echo $this->get_field_name( 'postnr' ); ?>">            
			<?php
				for ( $i = 1; $i <= 15; ++$i )
				echo "<option value='$i' " . ( $instance['postnr'] == $i ? "selected='selected'" : '' ) . ">$i</option>";
			?>
			</select>
		</p>        

		<p style="background-color: #efefef;border:1px solid #ddd;padding:10px;">Custom query will generate thumbnails only if TimThumb is the active thumbnail script. All Gabfire themes have TimThumb option enabled by default. However, if you use WP Post Thumbnails option, then thumbnails CANNOT be generated by this widget.</p>
		
		<p style="float:left;width:235px">
			<label for="<?php echo $this->get_field_id( 'd_thumb' ); ?>">Show Thumbnails</label><br />
			<select id="<?php echo $this->get_field_id( 'd_thumb' ); ?>" name="<?php echo $this->get_field_name( 'd_thumb' ); ?>" style="width:235px">
				<option value="1" <?php if ( '1' == $instance['d_thumb'] ) echo 'selected="selected"'; ?>>Yes</option>
				<option value="0" <?php if ( '0' == $instance['d_thumb'] ) echo 'selected="selected"'; ?>>No</option>    
			</select>
		</p>
		
		<p style="float:right;width:235px">
			<label for="<?php echo $this->get_field_id( 'video' ); ?>">Show Videos</label><br />
			<select id="<?php echo $this->get_field_id( 'video' ); ?>" name="<?php echo $this->get_field_name( 'video' ); ?>" style="width:235px">
				<option value="1" <?php if ( '1' == $instance['video'] ) echo 'selected="selected"'; ?>>Yes</option>
				<option value="0" <?php if ( '0' == $instance['video'] ) echo 'selected="selected"'; ?>>No</option>    
			</select>
		</p>   		
		
		<p style="float:left;width:235px">
			<label for="<?php echo $this->get_field_id('media_w'); ?>">Width of Thumbnail (ex: 50 or 300)</label>
			<input class="widefat"  id="<?php echo $this->get_field_id('media_w'); ?>" name="<?php echo $this->get_field_name('media_w'); ?>" type="text" value="<?php echo esc_attr( $instance['media_w'] ); ?>" />
		</p>

		<p style="float:right;width:235px">
			<label for="<?php echo $this->get_field_id('media_h'); ?>">Height of Thumbnail (ex: 50 or 300)</label>
			<input class="widefat"  id="<?php echo $this->get_field_id('media_h'); ?>" name="<?php echo $this->get_field_name('media_h'); ?>" type="text" value="<?php echo esc_attr( $instance['media_h'] ); ?>" />
		</p>
		
		<p style="float:left;width:235px">
			<label for="<?php echo $this->get_field_id( 'thumbalign' ); ?>">Thumbnail Alignment</label><br />
			<select id="<?php echo $this->get_field_id( 'thumbalign' ); ?>" name="<?php echo $this->get_field_name( 'thumbalign' ); ?>" style="width:235px">
				<option value="alignleft" <?php if ( 'alignleft' == $instance['thumbalign'] ) echo 'selected="selected"'; ?>>Left</option>
				<option value="alignright" <?php if ( 'alignright' == $instance['thumbalign'] ) echo 'selected="selected"'; ?>>Right</option>
				<option value="aligncenter" <?php  if ( 'aligncenter' == $instance['thumbalign'] ) echo 'selected="selected"'; ?>>Center</option>            
			</select>
		</p>
		
		<p style="float:right;width:235px">
			<label for="<?php echo $this->get_field_id('excerpt_l'); ?>"># of Words in Excerpt (ex:35 / Max: 55)</label>
			<input class="widefat"  id="<?php echo $this->get_field_id('excerpt_l'); ?>" name="<?php echo $this->get_field_name('excerpt_l'); ?>" type="text" value="<?php echo esc_attr( $instance['excerpt_l'] ); ?>" />
		</p> 
		
		<div style="clear:both"></div>
		
		<p>
			<label for="<?php echo $this->get_field_id( 'swap' ); ?>">Thumbnail and Title Position</label><br />
			<select id="<?php echo $this->get_field_id( 'swap' ); ?>" name="<?php echo $this->get_field_name( 'swap' ); ?>" style="width:480px">
				<option value="1" <?php if ( '1' == $instance['swap'] ) echo 'selected="selected"'; ?>>Show title first then thumbnail</option>
				<option value="0" <?php if ( '0' == $instance['swap'] ) echo 'selected="selected"'; ?>>Show thumbnail first then title</option>
			</select>
		</p>			
				
		<p>
			<label for="<?php echo $this->get_field_id( 'postmeta' ); ?>">Display post meta below excerpt</label> 
			<select id="<?php echo $this->get_field_id( 'postmeta' ); ?>" name="<?php echo $this->get_field_name( 'postmeta' ); ?>">
				<option value="1" <?php if ( '1' == $instance['postmeta'] ) echo 'selected="selected"'; ?>>Enable</option>
				<option value="0" <?php if ( '0' == $instance['postmeta'] ) echo 'selected="selected"'; ?>>Disable</option>    
			</select>
		</p>
		
		<h4>Advanced - For Developers Only</h4>
		
		<p style="background-color: #efefef;border:1px solid #ddd;padding:10px;">For developers only. Do not edit any of classes below unless you know what you are doing.</p>
		
		<p style="float:left;width:235px">
			<label for="<?php echo $this->get_field_id('postcls'); ?>">CSS Class of Post Wrapper Div</label>
			<input class="widefat"  id="<?php echo $this->get_field_id('postcls'); ?>" name="<?php echo $this->get_field_name('postcls'); ?>" type="text" value="<?php echo esc_attr( $instance['postcls'] ); ?>" />
		</p>

		<p style="float:right;width:235px">
			<label for="<?php echo $this->get_field_id('titlecls'); ?>">CSS Class of Post Title</label>
			<input class="widefat"  id="<?php echo $this->get_field_id('titlecls'); ?>" name="<?php echo $this->get_field_name('titlecls'); ?>" type="text" value="<?php echo esc_attr( $instance['titlecls'] ); ?>" />
		</p>		
<?php
	}
}

function customquery_register(){
	register_widget('gab_custom_widget');
}
add_action( 'widgets_init', 'customquery_register' );

if ( ! function_exists( 'entrynr_perCat' ) ) {
	function entrynr_perCat( $query ) {
		if ( is_admin() || ! $query->is_main_query() )
			return;

		if ( is_category(explode(',', of_get_option('of_ac_cslide'))) ) {
			$query->set( 'posts_per_page', 9 );
			return;
		}
		
		if ( is_category(explode(',', of_get_option('of_ac_mag'))) ) {
			$query->set( 'posts_per_page', 12 );
			return;
		}
		
		if ( is_category(explode(',', of_get_option('of_ac_2col'))) ) {
			$query->set( 'posts_per_page', 12 );
			return;
		}
		
		if ( is_category(explode(',', of_get_option('of_ac_mediatmp'))) ) {
			$query->set( 'posts_per_page', 6 );
			return;
		}
		
		if ( is_category(explode(',', of_get_option('of_ac_4col'))) ) {
			$query->set( 'posts_per_page', 12 );
			return;
		}	
	}
	add_action( 'pre_get_posts', 'entrynr_perCat', 1 );
}