		<?php 
		
		/* ******************** IF HEADER WITH A SINGLE IMAGE IS ACTIVATED *************************** */
		if(of_get_option('of_header_type') == 'singleimage') { ?>
			<a href="<?php echo home_url('/'); ?>" title="<?php bloginfo('description'); ?>">
				<img src="<?php echo of_get_option('of_himageurl'); ?>" id="header_banner" alt="<?php bloginfo('name'); ?>" title="<?php bloginfo('name'); ?>"/>
			</a>
		<?php } 
		
		/* ******************** IF HEADER LOGO - BANNER IS ACTIVATED ********************************* */
		elseif(of_get_option('of_header_type') == 'logobanner') { ?>
			<div class="left-2col">
				<div class="logo" style="padding:<?php echo of_get_option('of_padding_top', 0); ?>px 0px <?php echo of_get_option('of_padding_bottom', 0); ?>px <?php echo of_get_option('of_padding_left', 0); ?>px;">	
					<?php if ( of_get_option('of_logo_type', 'text') == 'image') { ?>
						<h1>
							<a href="<?php echo home_url('/'); ?>" title="<?php bloginfo('description'); ?>">
								<img src="<?php echo of_get_option('of_logo'); ?>" alt="<?php bloginfo('name'); ?>" title="<?php bloginfo('name'); ?>"/>
							</a>
						</h1>
					<?php } else { ?>
						<h1>
							<a href="<?php echo home_url('/'); ?>" title="<?php bloginfo('description'); ?>"><?php echo of_get_option('of_logo1'); ?></a>
							<span><a href="<?php echo home_url('/'); ?>" title="<?php bloginfo('description'); ?>"><?php echo of_get_option('of_logo2'); ?></a></span>
						</h1>
					<?php } ?>
				</div><!-- .logo -->
			</div><!-- .left-2col -->
			
			<div class="right-2col" <?php if (of_get_option('of_ac_adm1') <> '') { ?>style="padding-top:<?php echo of_get_option('of_ac_adm1'); ?>px"<?php } ?>>
				<div class="banner">
					<?php gab_categoryad('header_468x60'); ?>
				</div>
			</div><!-- .right-2col -->

		<?php } elseif(of_get_option('of_header_type') == 'adlogoad') {
		/* ******************** IF HEADER [AD - LOGO - AD] ACTIVATED ********************************* */	
		?>
		
			<div class="col-3col left ad" <?php if (of_get_option('of_ac_adm1') <> '') { ?>style="padding-top:<?php echo of_get_option('of_ac_adm1'); ?>px"<?php } ?>>
				<?php gab_categoryad('header_234x90-left'); ?>
			</div><!-- .col-3col -->
			
			<div class="mid-3col">
				<div class="logo" style="padding:<?php echo of_get_option('of_padding_top', 0); ?>px 0px <?php echo of_get_option('of_padding_bottom', 0); ?>px <?php echo of_get_option('of_padding_left', 0); ?>px;">	
					<?php if ( of_get_option('of_logo_type', 'text') == 'image') { ?>
						<h1>
							<a href="<?php echo home_url('/'); ?>" title="<?php bloginfo('description'); ?>">
								<img src="<?php echo of_get_option('of_logo'); ?>" alt="<?php bloginfo('name'); ?>" title="<?php bloginfo('name'); ?>"/>
							</a>
						</h1>
					<?php } else { ?>
						<h1>
							<a href="<?php echo home_url('/'); ?>" title="<?php bloginfo('description'); ?>"><?php echo of_get_option('of_logo1'); ?></a>
							<span><a href="<?php echo home_url('/'); ?>" title="<?php bloginfo('description'); ?>"><?php echo of_get_option('of_logo2'); ?></a></span>
						</h1>
					<?php } ?>
				</div><!-- .logo -->
			</div><!-- .mid-3col -->	

			<div class="col-3col right ad" <?php if (of_get_option('of_ac_adm1') <> '') { ?>style="padding-top:<?php echo of_get_option('of_ac_adm1'); ?>px"<?php } ?>>			
				<?php gab_categoryad('header_234x90-right'); ?>
			</div><!-- .col-3col -->		
		
		<?php } else {
		/* ******************** DEFAULT HEADER ********************************* */	
		?>
			<div class="left-2col">
				<div class="logo" style="padding:<?php echo of_get_option('of_padding_top', 0); ?>px 0px <?php echo of_get_option('of_padding_bottom', 0); ?>px <?php echo of_get_option('of_padding_left', 0); ?>px;">	
					<?php if ( of_get_option('of_logo_type', 'text') == 'image') { ?>
						<h1>
							<a href="<?php echo home_url('/'); ?>" title="<?php bloginfo('description'); ?>">
								<img src="<?php echo of_get_option('of_logo'); ?>" alt="<?php bloginfo('name'); ?>" title="<?php bloginfo('name'); ?>"/>
							</a>
						</h1>
					<?php } else { ?>
						<h1>
							<a href="<?php echo home_url('/'); ?>" title="<?php bloginfo('description'); ?>"><?php echo of_get_option('of_logo1'); ?></a>
							<span><a href="<?php echo home_url('/'); ?>" title="<?php bloginfo('description'); ?>"><?php echo of_get_option('of_logo2'); ?></a></span>
						</h1>
					<?php } ?>
				</div><!-- .logo -->
			</div><!-- .left-2col -->

			<div class="right-2col" style="margin-top:<?php echo of_get_option('of_searchmrgn'); ?>px">
				<?php get_search_form(); ?>
			</div><!-- .right-2col -->
		<?php } ?>