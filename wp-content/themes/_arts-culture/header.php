<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head profile="http://gmpg.org/xfn/11">

<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
	
<title><?php gab_title(); ?></title>

	<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />

	<link rel="alternate" type="application/rss+xml" title="RSS 2.0" href="<?php if ( of_get_option('of_rssaddr') <> '' ) { echo of_get_option('of_rssaddr'); } else { echo bloginfo('rss2_url'); } ?>" />	
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" /> 
	<?php
		if ( is_singular() && get_option( 'thread_comments' ) )
			wp_enqueue_script( 'comment-reply' );
		wp_head();
	?>	
		
	<?php if(file_exists(TEMPLATEPATH . '/custom.css')) { ?>
		<link rel="stylesheet" type="text/css" media="all" href="<?php echo get_template_directory_uri() ; ?>/custom.css" />
	<?php } ?>	
	
	<?php if(of_get_option('of_ac_customcolors', 0) == 1) { include (TEMPLATEPATH . '/typography.php'); } ?>
	
</head>

<body <?php body_class(); ?>>
<?php global $do_not_duplicate; ?>

<div id="header">
	<div class="wrapper">
		<?php if (of_get_option('of_ac_ad1') !== '') { ?>
			<div class="headerad728">
				<?php gab_categoryad('header_728x90'); ?>
			</div>
		<?php } ?>
		
		<?php if (of_get_option('of_ac_ad2') !== '') { ?>
			<div class="headerad234">
				<?php gab_categoryad('header_234x90'); ?>
			</div>
		<?php } ?>
		
		<div class="clear"></div>
		
	<?php include (TEMPLATEPATH . '/header-types.php'); ?>
	</div><!-- .wrapper -->
</div><!-- #header -->

<div id="mainmenu" class="wrapper">
	<ul class="mainnav dropdown">
		<li class="first<?php if(is_home() ) { ?> current-cat<?php } ?>"><a href="<?php echo home_url('/'); ?>" title="<?php bloginfo('description'); ?>"><?php _e('Home','artcltr'); ?></a></li>
		<?php
		if(of_get_option('of_nav1', 0) == 1) { 
			wp_nav_menu( array('theme_location' => 'primary', 'container' => false, 'items_wrap' => '%3$s'));
		} else {
			wp_list_categories('orderby='. of_get_option('of_order_cats') .'&order='. of_get_option('of_sort_cats') .'&title_li=&exclude='. of_get_option('of_ex_cats'));
		}
		?>
	</ul>
</div><!-- #mainmenu -->

<span class="belowmenu wrapper"></span>

<ul class="galleries_list dropdown">
<?php 
if(of_get_option('of_nav2', 0) == 1) { 
	wp_nav_menu( array('theme_location' => 'secondary', 'container' => false, 'items_wrap' => '%3$s'));
} else { ?>
	<li class="first"><?php _e('Trending Topics','artcltr'); ?></li>
	<li><?php wp_tag_cloud( 'orderby=count&number=4&smallest=13&largest=13&unit=px' ); ?></li>
<?php } ?>
</ul>
<div class="clear"></div>

<?php if(is_single() or is_page()) { ?>
<hr class="wrapper separator" />
<?php } ?>