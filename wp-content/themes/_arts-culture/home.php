<?php get_header(); 
$do_not_duplicate = '';
?>
<?php if (intval(of_get_option('of_ac_nrfea', 1)) > 0 ) { ?>
	<div id="featured_posts">
		<div class="wrapper">

			<div id="featured-leftcol">
				<div id="featured-slider">
					<?php 
					$count = 1;
					if ( of_get_option('of_ac_fea_recent', 0) == 1 ) {
						$args = array(
						   'posts_per_page' => of_get_option('of_ac_nrfea' , 5)
						);
					} else {
						if ( of_get_option('of_ac_fea_tag') <> '' ) {
							$args = array(
							  'post_type' => 'any',
							  'posts_per_page' => of_get_option('of_ac_nrfea' , 5),
							  'tag' => of_get_option('of_ac_fea_tag')
							);
						} elseif ( of_get_option('of_ac_fea_cf', 0) == 1 ) {
							$args = array(
							  'post_type' => 'any',
							  'posts_per_page' => of_get_option('of_ac_nrfea' , 5),
							  'meta_key' => 'featured', 
							  'meta_value' => 'true'
							);
						} else {
							$args = array(
							  'posts_per_page' => of_get_option('of_ac_nrfea' , 5), 
							  'cat' => of_get_option('of_ac_fea_cat')
							);				
						}
					}
					$gab_query = new WP_Query();$gab_query->query($args); 
					while ($gab_query->have_posts()) : $gab_query->the_post();
					if (of_get_option('of_dnd') == 1) { $do_not_duplicate[] = $post->ID; }
					?>
					<div class="item" id="post-<?php the_ID(); ?>">
					
						<div class="slidemedia">				
							<?php 
							gab_media(array(
								'name' => 'ac-featured', 
								'imgtag' => 1,
								'link' => 1,
								'enable_video' => 1, 
								'catch_image' => of_get_option('of_catch_img', 0),
								'video_id' => 'featured', 
								'enable_thumb' => 1, 
								'resize_type' => 'c', /* c to crop, h to resize only height, w to resize only width */
								'media_width' => 332, 
								'media_height' => 212, 
								'thumb_align' => 'featured_media',
								'enable_default' => 0
							)); 										
							?>
						</div><!-- end of sliderphoto/video -->		

							<div class="slidetext">
								<h2 class="posttitle">
									<a href="<?php the_permalink() ?>" rel="bookmark" title="<?php printf( esc_attr__( 'Permalink to %s', 'artcltr' ), the_title_attribute( 'echo=0' ) ); ?>"><?php the_title(); ?></a>
								</h2>
								
								<?php $excerpt = string_limit_words(get_the_excerpt(), 45); ?>
								<p><?php echo $excerpt; if(str_word_count($excerpt) > 45) { echo '&hellip;'; } ?></p>
							</div>
					</div><!-- /item -->
					<?php $count++; endwhile; wp_reset_query(); ?>
				</div><!-- /featurd-slider -->
				
				<ul id="nav">
					<?php
					$count = 1;
					$gab_query = new WP_Query();$gab_query->query($args); 
					while ($gab_query->have_posts()) : $gab_query->the_post();
					?>
					<li <?php if($count % 6 == 0) { echo 'class="last"'; } ?>>
					<a href="#">
						<?php 
						gab_media(array(
							'name' => 'ac-feathumb', 
							'imgtag' => 1,
							'link' => 0,
							'enable_video' => 0, 
							'catch_image' => of_get_option('of_catch_img', 0),
							'enable_thumb' => 1, 
							'resize_type' => 'c', /* c to crop, h to resize only height, w to resize only width */
							'media_width' => 82, 
							'media_height' => 54, 
							'thumb_align' => 'fea_thumb', 
							'enable_default' => 1,
							'default_name' => 'feathumb.jpg'
						)); 										
						?>
					</a>
					</li>
					<?php $count++; endwhile; wp_reset_query(); ?>
				</ul>
			</div><!-- /featurd-leftcol -->
			
			<div id="featured-rightcol">
<!-- 				<ul class="featuredposts">
				<?php
				$count = 1; 
				$args = array(
				  'posts_per_page' => of_get_option('of_ac_feanr2'), 
				  'cat' => of_get_option('of_ac_feacat2')
				);	
				$gab_query = new WP_Query();$gab_query->query($args); 
				while ($gab_query->have_posts()) : $gab_query->the_post();						
				if (of_get_option('of_dnd') == 1) { $do_not_duplicate[] = $post->ID; }
				$title2 = get_post_meta($post->ID, 'title2', true);
				?>		
				<li>
					<?php if($title2 !== '') { ?><span class="subtitle"><?php echo $title2; ?></span><?php } ?>
					<a class="posttitle" href="<?php the_permalink() ?>" rel="bookmark" title="<?php printf( esc_attr__( 'Permalink to %s', 'artcltr' ), the_title_attribute( 'echo=0' ) ); ?>">
						<?php gab_posttitle(32,'&hellip;'); ?>
					</a>
				</li>
				<?php $count++; endwhile; wp_reset_query(); ?>
				</ul>  -->
				<!-- <a class="morebutton" href="<?php echo get_category_link(of_get_option('of_ac_feacat2'));?>">
					<?php _e('View More','artcltr'); ?>
				</a>			
				 -->
				<h2>Subscribe Now!</h2>
				<p class="subscribe-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
				tempor incididunt ut labore et dolore magna aliqua. </p>
				<p class="subscribe-text">>Ut enim ad minim veniam,
				quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
				consequat.</p>
				<a class="morebutton" href="">
					Subscribe to Fusion 
				</a>			
			</div><!-- /featured-rightcol -->
		</div><!-- /wrapper -->
	</div><!-- /featured_posts -->

	<div class="clear"></div>
<?php } ?>

<div id="homecontent">
	<div class="wrapper">
		<div class="leftcol">
							<?php gab_dynamic_sidebar('LeftCol1'); ?>
		
							<?php if (intval(of_get_option('of_ac_nr1', 1)) > 0 ) { ?>
								<!-- LEFT COLUMN CATEGORY BLOCK 1 -->
								<div class="postcontainer">
									<?php
									echo '<p class="catname">';
										if(of_get_option('of_ac_capimg1') <> "" ) { 
											echo '<a href="' . get_category_link(of_get_option('of_ac_cat1')) . '">';
												echo '<img src="'.of_get_option('of_ac_capimg1').'" alt="" />';
											echo '</a>';
										} elseif ((of_get_option('of_cap1_1') <> '' ) or (of_get_option('of_cap1_2') <> '')) {
											echo '<span class="firstrow"><a href="'. get_category_link(of_get_option('of_ac_cat1')) .'">' . of_get_option('of_cap1_1') .'</a></span>';
											echo '<span class="secondrow"><a href="'. get_category_link(of_get_option('of_ac_cat1')) .'">' . of_get_option('of_cap1_2') . '</a></span>';
										} else {
											echo '<a href="'. get_category_link(of_get_option('of_ac_cat1')) .'">'. get_cat_name(of_get_option('of_ac_cat1')) .'</a>';
										}
									echo '</p>';
									
									$count = 1; 
									$args = array(
									  'posts_per_page' => of_get_option('of_ac_nr1'),
									  'post__not_in'=> $do_not_duplicate,									  
									  'cat' => of_get_option('of_ac_cat1')
									);	
									$gab_query = new WP_Query();$gab_query->query($args); 
									while ($gab_query->have_posts()) : $gab_query->the_post();						
									if (of_get_option('of_dnd') == 1) { $do_not_duplicate[] = $post->ID; }
									?>
										<div class="featuredpost<?php if($count == of_get_option('of_ac_nr1')) { echo ' lastpost'; } ?>">
											<?php
											gab_media(array(
												'name' => 'ac-belowfeanarrow', 
												'imgtag' => 1,
												'link' => 1,
												'enable_video' => 1, 
												'catch_image' => of_get_option('of_catch_img', 0),
												'video_id' => 'belowfea1', 
												'enable_thumb' => 1,
												'resize_type' => 'c', /* c to crop, h to resize only height, w to resize only width */
												'media_width' => 217, 
												'media_height' => 140, 
												'thumb_align' => 'aligncenter',
												'enable_default' => 0
											)); 
											?>
													
											<h2 class="posttitle s_title">
												<a href="<?php the_permalink() ?>" rel="bookmark" title="<?php printf( esc_attr__( 'Permalink to %s', 'artcltr' ), the_title_attribute( 'echo=0' ) ); ?>" >
													<?php the_title(); ?>
												</a>
											</h2>

											<?php echo '<p>' . string_limit_words(get_the_excerpt(), 7) . '&hellip;</p>'; ?>							
										</div>
									<?php 
									$count++; endwhile; wp_reset_query();
									?>
								</div><!-- /postcontainer -->
							<?php } ?>
							
							<?php gab_dynamic_sidebar('LeftCol2'); ?>
							
							<?php if (intval(of_get_option('of_ac_nr2', 1)) > 0 ) { ?>
								<!-- LEFT COLUMN CATEGORY BLOCK 2 -->
								<div class="postcontainer">
									<?php
									echo '<p class="catname">';
										if(of_get_option('of_ac_capimg2') <> '' ) { 
											echo '<a href="' . get_category_link(of_get_option('of_ac_cat2')) . '">';
												echo '<img src="'.of_get_option('of_ac_capimg2').'" alt="" />';
											echo '</a>';
										} elseif ((of_get_option('of_cap2_1') <> '' ) or (of_get_option('of_cap2_2') <> '')) {
											echo '<span class="firstrow"><a href="'. get_category_link(of_get_option('of_ac_cat2')) .'">' . of_get_option('of_cap2_1') .'</a></span>';
											echo '<span class="secondrow"><a href="'. get_category_link(of_get_option('of_ac_cat2')) .'">' . of_get_option('of_cap2_2') . '</a></span>';
										} else {
											echo '<a href="'. get_category_link(of_get_option('of_ac_cat2')) .'">'. get_cat_name(of_get_option('of_ac_cat2')) .'</a>';
										}
									echo '</p>';
									
									$count = 1; 
									$args = array(
									  'posts_per_page' => of_get_option('of_ac_nr2'), 
									  'post__not_in'=> $do_not_duplicate,
									  'cat' => of_get_option('of_ac_cat2')
									);	
									$gab_query = new WP_Query();$gab_query->query($args); 
									while ($gab_query->have_posts()) : $gab_query->the_post();						
									if (of_get_option('of_dnd') == 1) { $do_not_duplicate[] = $post->ID; }
									?>
										<div class="featuredpost<?php if($count == of_get_option('of_ac_nr2')) { echo ' lastpost'; } ?>">
											<?php
											gab_media(array(
												'name' => 'ac-belowfeanarrow', 
												'imgtag' => 1,
												'link' => 1,
												'enable_video' => 1, 
												'catch_image' => of_get_option('of_catch_img', 0),
												'video_id' => 'belowfea2', 
												'enable_thumb' => 1,
												'resize_type' => 'c', /* c to crop, h to resize only height, w to resize only width */
												'media_width' => 217, 
												'media_height' => 140, 
												'thumb_align' => 'aligncenter',
												'enable_default' => 0
											)); 
											?>
													
											<h2 class="posttitle s_title">
												<a href="<?php the_permalink() ?>" rel="bookmark" title="<?php printf( esc_attr__( 'Permalink to %s', 'artcltr' ), the_title_attribute( 'echo=0' ) ); ?>" >
													<?php the_title(); ?>
												</a>
											</h2>

											<?php echo '<p>' . string_limit_words(get_the_excerpt(), 12) . '&hellip;</p>'; ?>							
										</div>
									<?php 
									$count++; endwhile; wp_reset_query();
									?>
								</div><!-- /postcontainer -->
							<?php } ?>
							
							<?php gab_dynamic_sidebar('LeftCol3'); ?>
							
							<?php if (intval(of_get_option('of_ac_nr3', 1)) > 0 ) { ?>
								<!-- LEFT COLUMN CATEGORY BLOCK 3 -->
								<div class="postcontainer">
									<?php
									echo '<p class="catname">';
										if(of_get_option('of_ac_capimg3') <> '' ) { 
											echo '<a href="' . get_category_link(of_get_option('of_ac_cat3')) . '">';
												echo '<img src="'.of_get_option('of_ac_capimg3').'" alt="" />';
											echo '</a>';
										} elseif ((of_get_option('of_cap3_1') <> '' ) or (of_get_option('of_cap3_2') <> '')) {
											echo '<span class="firstrow"><a href="'. get_category_link(of_get_option('of_ac_cat3')) .'">' . of_get_option('of_cap3_1') .'</a></span>';
											echo '<span class="secondrow"><a href="'. get_category_link(of_get_option('of_ac_cat3')) .'">' . of_get_option('of_cap3_2') . '</a></span>';
										} else {
											echo '<a href="'. get_category_link(of_get_option('of_ac_cat3')) .'">'. get_cat_name(of_get_option('of_ac_cat3')) .'</a>';
										}
									echo '</p>';
									
									$count = 1; 
									$args = array(
									  'posts_per_page' => of_get_option('of_ac_nr3'), 
									  'cat' => of_get_option('of_ac_cat3')
									);	
									$gab_query = new WP_Query();$gab_query->query($args); 
									while ($gab_query->have_posts()) : $gab_query->the_post();						
									if (of_get_option('of_dnd') == 1) { $do_not_duplicate[] = $post->ID; }
									?>
										<div class="featuredpost<?php if($count == of_get_option('of_ac_nr3')) { echo ' lastpost'; } ?>">
											<?php
											gab_media(array(
												'name' => 'ac-belowfeanarrow', 
												'imgtag' => 1,
												'link' => 1,
												'enable_video' => 1, 
												'catch_image' => of_get_option('of_catch_img', 0),
												'video_id' => 'belowfea3', 
												'enable_thumb' => 1,
												'resize_type' => 'c', /* c to crop, h to resize only height, w to resize only width */
												'media_width' => 217, 
												'media_height' => 140, 
												'thumb_align' => 'aligncenter',
												'enable_default' => 0
											)); 
											?>
													
											<h2 class="posttitle s_title">
												<a href="<?php the_permalink() ?>" rel="bookmark" title="<?php printf( esc_attr__( 'Permalink to %s', 'artcltr' ), the_title_attribute( 'echo=0' ) ); ?>" >
													<?php the_title(); ?>
												</a>
											</h2>

											<?php echo '<p>' . string_limit_words(get_the_excerpt(), 12) . '&hellip;</p>'; ?>						
										</div>
									<?php 
									$count++; endwhile; wp_reset_query();
									?>
								</div><!-- /postcontainer -->
							<?php } ?>
							
							<?php gab_dynamic_sidebar('LeftCol4'); ?>
							
							<?php if (intval(of_get_option('of_ac_nrn1', 1)) > 0 ) { ?>
								<!-- LEFT COLUMN CATEGORY BLOCK 4 -->
								<div class="postcontainer">
									<?php
									echo '<p class="catname">';
										if(of_get_option('of_ac_capimgn1') <> "" ) { 
											echo '<a href="' . get_category_link(of_get_option('of_ac_catn1')) . '">';
												echo '<img src="'.of_get_option('of_ac_capimgn1').'" alt="" />';
											echo '</a>';
										} elseif ((of_get_option('of_capn1_1') <> '' ) or (of_get_option('of_capn1_2') <> '')) {
											echo '<span class="firstrow"><a href="'. get_category_link(of_get_option('of_ac_catn1')) .'">' . of_get_option('of_capn1_1') .'</a></span>';
											echo '<span class="secondrow"><a href="'. get_category_link(of_get_option('of_ac_catn1')) .'">' . of_get_option('of_capn1_2') . '</a></span>';
										} else {
											echo '<a href="'. get_category_link(of_get_option('of_ac_catn1')) .'">'. get_cat_name(of_get_option('of_ac_catn1')) .'</a>';
										}
									echo '</p>';
									
									$count = 1; 
									$args = array(
									  'posts_per_page' => of_get_option('of_ac_nrn1'),
									  'post__not_in'=> $do_not_duplicate,									  
									  'cat' => of_get_option('of_ac_catn1')
									);	
									$gab_query = new WP_Query();$gab_query->query($args); 
									while ($gab_query->have_posts()) : $gab_query->the_post();						
									if (of_get_option('of_dnd') == 1) { $do_not_duplicate[] = $post->ID; }
									?>
											<?php
											if ($count == 1) {
											gab_media(array(
												'name' => 'ac-belowfeanarrow', 
												'imgtag' => 1,
												'link' => 1,
												'enable_video' => 1, 
												'catch_image' => of_get_option('of_catch_img', 0),
												'video_id' => 'belowfea4', 
												'enable_thumb' => 1,
												'resize_type' => 'c', /* c to crop, h to resize only height, w to resize only width */
												'media_width' => 217, 
												'media_height' => 140, 
												'thumb_align' => 'aligncenter',
												'enable_default' => 0
											)); 
											}
											?>
													
											<h2 class="posttitle list_title">
												<a href="<?php the_permalink() ?>" rel="bookmark" title="<?php printf( esc_attr__( 'Permalink to %s', 'artcltr' ), the_title_attribute( 'echo=0' ) ); ?>" >
													<?php gab_posttitle(32,'&hellip;'); ?>
												</a>
											</h2>

									<?php 
									$count++; endwhile; wp_reset_query();
									?>
								</div><!-- /postcontainer -->
							<?php } ?>
							
							<?php gab_dynamic_sidebar('LeftCol5'); ?>
							
							<?php if (intval(of_get_option('of_ac_nrn2', 1)) > 0 ) { ?>
								<!-- LEFT COLUMN CATEGORY BLOCK 5 -->
								<div class="postcontainer last">
									<?php
									echo '<p class="catname">';
										if(of_get_option('of_ac_capimgn2') <> "" ) { 
											echo '<a href="' . get_category_link(of_get_option('of_ac_catn2')) . '">';
												echo '<img src="'.of_get_option('of_ac_capimgn2').'" alt="" />';
											echo '</a>';
										} elseif ((of_get_option('of_capn2_1') <> '' ) or (of_get_option('of_capn2_2') <> '')) {
											echo '<span class="firstrow"><a href="'. get_category_link(of_get_option('of_ac_catn2')) .'">' . of_get_option('of_capn2_1') .'</a></span>';
											echo '<span class="secondrow"><a href="'. get_category_link(of_get_option('of_ac_catn2')) .'">' . of_get_option('of_capn2_2') . '</a></span>';
										} else {
											echo '<a href="'. get_category_link(of_get_option('of_ac_catn2')) .'">'. get_cat_name(of_get_option('of_ac_catn2')) .'</a>';
										}
									echo '</p>';
									
									$count = 1; 
									$args = array(
									  'posts_per_page' => of_get_option('of_ac_nrn2'),
									  'post__not_in'=> $do_not_duplicate,									  
									  'cat' => of_get_option('of_ac_catn2')
									);	
									$gab_query = new WP_Query();$gab_query->query($args); 
									while ($gab_query->have_posts()) : $gab_query->the_post();						
									if (of_get_option('of_dnd') == 1) { $do_not_duplicate[] = $post->ID; }
									?>
											<?php
											if ($count == 1) {
											gab_media(array(
												'name' => 'ac-belowfeanarrow', 
												'imgtag' => 1,
												'link' => 1,
												'enable_video' => 1, 
												'catch_image' => of_get_option('of_catch_img', 0),
												'video_id' => 'belowfea5', 
												'enable_thumb' => 1,
												'resize_type' => 'c', /* c to crop, h to resize only height, w to resize only width */
												'media_width' => 217, 
												'media_height' => 140, 
												'thumb_align' => 'aligncenter',
												'enable_default' => 0
											)); 
											}
											?>
											<h2 class="posttitle list_title">
												<a href="<?php the_permalink() ?>" rel="bookmark" title="<?php printf( esc_attr__( 'Permalink to %s', 'artcltr' ), the_title_attribute( 'echo=0' ) ); ?>" >
													<?php gab_posttitle(32,'&hellip;'); ?>
												</a>
											</h2>
									<?php 
									$count++; endwhile; wp_reset_query();
									?>
								</div><!-- /postcontainer -->	
							<?php } ?>
							
							<?php gab_dynamic_sidebar('LeftCol6'); ?>
							
		</div><!-- /leftcol -->
		
		<div class="midcol">
								<?php gab_dynamic_sidebar('MidCol1'); ?>
								
								<?php if (intval(of_get_option('of_ac_nr4', 1) ) > 0 ) { ?>
									<div class="colwrapper">
										<?php
										if (of_get_option('of_ac_midslogan') !== '') { 
											echo '<p class="mid-slogan">' . stripslashes(of_get_option('of_ac_midslogan')) . '</p>'; 
										} ?>
										
										<?php
										$count = 1; 
										$args = array(
										  'posts_per_page' => of_get_option('of_ac_nr4'), 
										  'post__not_in'=> $do_not_duplicate,
										  'cat' => of_get_option('of_ac_cat4')
										);	
										$gab_query = new WP_Query();$gab_query->query($args); 
										while ($gab_query->have_posts()) : $gab_query->the_post();						
										if (of_get_option('of_dnd') == 1) { $do_not_duplicate[] = $post->ID; }
										?>
											<div class="featuredpost<?php if($count == of_get_option('of_ac_nr4')) { echo ' lastpost'; } ?>">
																				
												<?php
												gab_media(array(
													'name' => 'ac-belowfeamid1', 
													'imgtag' => 1,
													'link' => 1,
													'enable_video' => 1, 
													'catch_image' => of_get_option('of_catch_img', 0),
													'video_id' => 'belowfeamid1', 
													'enable_thumb' => 1,
													'resize_type' => 'c', /* c to crop, h to resize only height, w to resize only width */
													'media_width' => 479, 
													'media_height' => 300, 
													'thumb_align' => 'aligncenter',
													'enable_default' => 0
												)); 
												?>
												<h2 class="posttitle">
													<a href="<?php the_permalink() ?>" rel="bookmark" title="<?php printf( esc_attr__( 'Permalink to %s', 'artcltr' ), the_title_attribute( 'echo=0' ) ); ?>" >
														<?php the_title(); ?>
													</a>
												</h2>
												<?php echo '<p>' . string_limit_words(get_the_excerpt(), 16) . '&hellip;</p>'; ?>							
											</div>
										<?php 
										$count++; endwhile; wp_reset_query();
										?>
									</div><!-- colwrapper -->
								<?php } ?>
								
								<?php 
									if( of_get_option('of_ac_ad6') !== '' ) {  
										echo '<div class="homepagead">';
										include (TEMPLATEPATH . '/ads/home_468x60.php');
										echo '</div>';
									}
								?>

								<?php if (intval((of_get_option('of_ac_nr5', 1)) + (of_get_option('of_ac_nr6', 1)) ) > 0 ) { ?>
									<div class="colwrapper border-bottom">
										<div class="col left">
											
											<?php gab_dynamic_sidebar('MidCol2'); ?>
											
											<?php
											echo '<p class="catname">';
												if(of_get_option('of_ac_capimg5') <> '' ) { 
													echo '<a href="' . get_category_link(of_get_option('of_ac_cat5')) . '">';
														echo '<img src="'.of_get_option('of_ac_capimg5').'" alt="" />';
												echo '</a>';
											} elseif ((of_get_option('of_cap5_1') <> '' ) or (of_get_option('of_cap5_2') <> '')) {
												echo '<span class="firstrow"><a href="'. get_category_link(of_get_option('of_ac_cat5')) .'">' . of_get_option('of_cap5_1') .'</a></span>';
												echo '<span class="secondrow"><a href="'. get_category_link(of_get_option('of_ac_cat5')) .'">' . of_get_option('of_capn5_2') . '</a></span>';
											echo '<span>' . of_get_option('of_cap5_2') . '</span>';
												} else {
													echo '<a href="'. get_category_link(of_get_option('of_ac_cat5')) .'">'. get_cat_name(of_get_option('of_ac_cat5')) .'</a>';
												}
											echo '</p>';									
											
											$count = 1; 
											$args = array(
											  'posts_per_page' => of_get_option('of_ac_nr5'), 
											  'post__not_in'=> $do_not_duplicate,
											  'cat' => of_get_option('of_ac_cat5')
											);	
											$gab_query = new WP_Query();$gab_query->query($args); 
											while ($gab_query->have_posts()) : $gab_query->the_post();						
											if (of_get_option('of_dnd') == 1) { $do_not_duplicate[] = $post->ID; }
											if ($count == 1) {
												$name = 'ac-belowfeamid2';
												$width = 227;
												$height = 120;
												$align = 'aligncenter';
												$title_length = 40;
												$excerpt_length = 12;
												$title_class = 'posttitle';
											} else { 
												$name = 'ac-belowfeamid3';
												$width = 80;
												$height = 51;
												$align = 'alignleft';
												$title_length = 19;
												$excerpt_length = 6;
												$title_class = 'posttitle s_title';
											}
											?>
												<div class="featuredpost<?php if($count == of_get_option('of_ac_nr5')) { echo ' lastpost'; } ?>">
													<?php
													gab_media(array(
														'name' => $name, 
														'imgtag' => 1,
														'link' => 1,
														'enable_video' => 1, 
														'catch_image' => of_get_option('of_catch_img', 0),
														'video_id' => 'belowfeamid2', 
														'enable_thumb' => 1,
														'resize_type' => 'c', /* c to crop, h to resize only height, w to resize only width */
														'media_width' => $width, 
														'media_height' =>  $height, 
														'thumb_align' => $align,
														'enable_default' => 0
													)); 
													?>
													
													<h2 class="<?php echo $title_class; ?>">
														<a href="<?php the_permalink() ?>" rel="bookmark" title="<?php printf( esc_attr__( 'Permalink to %s', 'artcltr' ), the_title_attribute( 'echo=0' ) ); ?>" >
															<?php gab_posttitle($title_length,'&hellip;'); ?>
														</a>
													</h2>											
													
													<?php echo '<p>' . string_limit_words(get_the_excerpt(), $excerpt_length) . '&hellip;</p>'; ?>					
												</div>
											<?php 
											$count++; endwhile; wp_reset_query();
											?>
											<?php gab_dynamic_sidebar('MidCol3'); ?>
										</div>
										
										<div class="col right">
											<?php gab_dynamic_sidebar('MidCol4'); ?>
											
											<?php
											echo '<p class="catname">';
												if(of_get_option('of_ac_capimg6') <> '' ) { 
													echo '<a href="' . get_category_link(of_get_option('of_ac_cat6')) . '">';
														echo '<img src="'.of_get_option('of_ac_capimg6').'" alt="" />';
												echo '</a>';
											} elseif ((of_get_option('of_cap6_1') <> '' ) or (of_get_option('of_cap6_2') <> '')) {
												echo '<span class="firstrow"><a href="'. get_category_link(of_get_option('of_ac_cat6')) .'">' . of_get_option('of_cap6_1') .'</a></span>';
												echo '<span class="secondrow"><a href="'. get_category_link(of_get_option('of_ac_cat6')) .'">' . of_get_option('of_capn6_2') . '</a></span>';
											echo '<span>' . of_get_option('of_cap6_2') . '</span>';
												} else {
													echo '<a href="'. get_category_link(of_get_option('of_ac_cat6')) .'">'. get_cat_name(of_get_option('of_ac_cat6')) .'</a>';
												}
											echo '</p>';	
											
											$count = 1; 
											$args = array(
											  'posts_per_page' => of_get_option('of_ac_nr6'), 
											  'post__not_in'=> $do_not_duplicate,
											  'cat' => of_get_option('of_ac_cat6')
											);	
											$gab_query = new WP_Query();$gab_query->query($args); 
											while ($gab_query->have_posts()) : $gab_query->the_post();						
											if (of_get_option('of_dnd') == 1) { $do_not_duplicate[] = $post->ID; }
											if ($count == 1) {
												$name = 'ac-belowfeamid2';
												$width = 227;
												$height = 120;
												$align = 'aligncenter';
												$title_length = 40;
												$excerpt_length = 12;
												$title_class = 'posttitle';
											} else { 
												$name = 'ac-belowfeamid3';
												$width = 80;
												$height = 51;
												$align = 'alignleft';
												$title_length = 19;
												$excerpt_length = 6;
												$title_class = 'posttitle s_title';
											}
											?>
												<div class="featuredpost<?php if($count == of_get_option('of_ac_nr6')) { echo ' lastpost'; } ?>">
													<?php
													gab_media(array(
														'name' => $name, 
														'imgtag' => 1,
														'link' => 1,
														'enable_video' => 1, 
														'catch_image' => of_get_option('of_catch_img', 0),
														'video_id' => 'belowfeamid2', 
														'enable_thumb' => 1,
														'resize_type' => 'c', /* c to crop, h to resize only height, w to resize only width */
														'media_width' => $width, 
														'media_height' =>  $height, 
														'thumb_align' => $align,
														'enable_default' => 0
													)); 
													?>
													
													<h2 class="<?php echo $title_class; ?>">
														<a href="<?php the_permalink() ?>" rel="bookmark" title="<?php printf( esc_attr__( 'Permalink to %s', 'artcltr' ), the_title_attribute( 'echo=0' ) ); ?>" >
															<?php gab_posttitle($title_length,'&hellip;'); ?>
														</a>
													</h2>											
													
													<?php echo '<p>' . string_limit_words(get_the_excerpt(), $excerpt_length) . '&hellip;</p>'; ?>
												</div>
											<?php 
											$count++; endwhile; wp_reset_query();
											?>
											<?php gab_dynamic_sidebar('MidCol5'); ?>
										</div>
									</div><!-- colwrapper -->
								<?php } ?>
								
								<?php gab_dynamic_sidebar('MidCol6'); ?>
								
								<?php if (intval((of_get_option('of_ac_nr7', 1)) + (of_get_option('of_ac_nr8', 1)) ) > 0 ) { ?>
									<div class="colwrapper">		
										<div class="col left">
											<?php gab_dynamic_sidebar('MidCol7'); ?>
											<?php
											echo '<p class="catname">';
												if(of_get_option('of_ac_capimg7') <> '' ) { 
													echo '<a href="' . get_category_link(of_get_option('of_ac_cat7')) . '">';
														echo '<img src="'.of_get_option('of_ac_capimg7').'" alt="" />';
													echo '</a>';
												} elseif ((of_get_option('of_cap7_1') <> '' ) or (of_get_option('of_cap7_2') <> '')) {
													echo '<span class="firstrow"><a href="'. get_category_link(of_get_option('of_ac_cat7')) .'">' . of_get_option('of_cap7_1') .'</a></span>';
													echo '<span class="secondrow"><a href="'. get_category_link(of_get_option('of_ac_cat7')) .'">' . of_get_option('of_capn7_2') . '</a></span>';
												} else {
													echo '<a href="'. get_category_link(of_get_option('of_ac_cat7')) .'">'. get_cat_name(of_get_option('of_ac_cat7')) .'</a>';
												}
											echo '</p>';	
											
											$count = 1; 
											$args = array(
											  'posts_per_page' => of_get_option('of_ac_nr7'), 
											  'post__not_in'=> $do_not_duplicate,
											  'cat' => of_get_option('of_ac_cat7')
											);	
											$gab_query = new WP_Query();$gab_query->query($args); 
											while ($gab_query->have_posts()) : $gab_query->the_post();						
											if (of_get_option('of_dnd') == 1) { $do_not_duplicate[] = $post->ID; }
											if ($count == 1) {
												$name = 'ac-belowfeamid2';
												$width = 227;
												$height = 120;
												$align = 'aligncenter';
												$title_length = 40;
												$excerpt_length = 12;
												$title_class = 'posttitle';
											} else { 
												$name = 'ac-belowfeamid3';
												$width = 80;
												$height = 51;
												$align = 'alignleft';
												$title_length = 19;
												$excerpt_length = 6;
												$title_class = 'posttitle s_title';
											}
											?>
												<div class="featuredpost<?php if($count == of_get_option('of_ac_nr7')) { echo ' lastpost'; } ?>">
													<?php
													gab_media(array(
														'name' => $name, 
														'imgtag' => 1,
														'link' => 1,
														'enable_video' => 1, 
														'catch_image' => of_get_option('of_catch_img', 0),
														'video_id' => 'belowfeamid3', 
														'enable_thumb' => 1,
														'resize_type' => 'c', /* c to crop, h to resize only height, w to resize only width */
														'media_width' => $width, 
														'media_height' =>  $height, 
														'thumb_align' => $align,
														'enable_default' => 0
													)); 
													?>
													
													<h2 class="<?php echo $title_class; ?>">
														<a href="<?php the_permalink() ?>" rel="bookmark" title="<?php printf( esc_attr__( 'Permalink to %s', 'artcltr' ), the_title_attribute( 'echo=0' ) ); ?>" >
															<?php gab_posttitle($title_length,'&hellip;'); ?>
														</a>
													</h2>											
													
													<?php echo '<p>' . string_limit_words(get_the_excerpt(), $excerpt_length) . '&hellip;</p>'; ?>
												</div>
											<?php 
											$count++; endwhile; wp_reset_query();
											?>
											<?php gab_dynamic_sidebar('MidCol8'); ?>
										</div>
										
										<div class="col right">
											<?php gab_dynamic_sidebar('MidCol9'); ?>
											<?php
											echo '<p class="catname">';
												if(of_get_option('of_ac_capimg8') <> '' ) { 
													echo '<a href="' . get_category_link(of_get_option('of_ac_cat8')) . '">';
														echo '<img src="'.of_get_option('of_ac_capimg8').'" alt="" />';
													echo '</a>';
												} elseif ((of_get_option('of_cap8_1') <> '' ) or (of_get_option('of_cap8_2') <> '')) {
													echo '<span class="firstrow"><a href="'. get_category_link(of_get_option('of_ac_cat8')) .'">' . of_get_option('of_cap8_1') .'</a></span>';
													echo '<span class="secondrow"><a href="'. get_category_link(of_get_option('of_ac_cat8')) .'">' . of_get_option('of_capn8_2') . '</a></span>';
												} else {
													echo '<a href="'. get_category_link(of_get_option('of_ac_cat8')) .'">'. get_cat_name(of_get_option('of_ac_cat8')) .'</a>';
												}
											echo '</p>';	
											
											$count = 1; 
											$args = array(
											  'posts_per_page' => of_get_option('of_ac_nr8'), 
											  'post__not_in'=> $do_not_duplicate,
											  'cat' => of_get_option('of_ac_cat8')
											);	
											$gab_query = new WP_Query();$gab_query->query($args); 
											while ($gab_query->have_posts()) : $gab_query->the_post();						
											if (of_get_option('of_dnd') == 1) { $do_not_duplicate[] = $post->ID; }
											if ($count == 1) {
												$name = 'ac-belowfeamid2';
												$width = 227;
												$height = 120;
												$align = 'aligncenter';
												$title_length = 40;
												$excerpt_length = 12;
												$title_class = 'posttitle';
											} else { 
												$name = 'ac-belowfeamid3';
												$width = 80;
												$height = 51;
												$align = 'alignleft';
												$title_length = 19;
												$excerpt_length = 6;
												$title_class = 'posttitle s_title';
											}
											?>
												<div class="featuredpost<?php if($count == of_get_option('of_ac_nr8')) { echo ' lastpost'; } ?>">
													<?php
													gab_media(array(
														'name' => $name, 
														'imgtag' => 1,
														'link' => 1,
														'enable_video' => 1, 
														'catch_image' => of_get_option('of_catch_img', 0),
														'video_id' => 'belowfeamid3', 
														'enable_thumb' => 1,
														'resize_type' => 'c', /* c to crop, h to resize only height, w to resize only width */
														'media_width' => $width, 
														'media_height' =>  $height, 
														'thumb_align' => $align,
														'enable_default' => 0
													)); 
													?>
													
													<h2 class="<?php echo $title_class; ?>">
														<a href="<?php the_permalink() ?>" rel="bookmark" title="<?php printf( esc_attr__( 'Permalink to %s', 'artcltr' ), the_title_attribute( 'echo=0' ) ); ?>" >
															
															<?php gab_posttitle($title_length,'&hellip;'); ?>
														</a>
													</h2>											
													
													<?php echo '<p>' . string_limit_words(get_the_excerpt(), $excerpt_length) . '&hellip;</p>'; ?>
												</div>
											<?php 
											$count++; endwhile; wp_reset_query();
											?>
											<?php gab_dynamic_sidebar('MidCol10'); ?>
										</div>		
									</div><!-- colwrapper -->
								<?php } ?>
								
								
								
									<?php gab_dynamic_sidebar('MidCol11'); ?>
									
									<?php if (intval(of_get_option('of_ac_nr9', 1)) > 0 ) { ?>
										<div id="bottom-slider">
											<?php
											$count = 1; 
											$args = array(
											  'posts_per_page' => of_get_option('of_ac_nr9'), 
											  'post__not_in'=> $do_not_duplicate,
											  'cat' => of_get_option('of_ac_cat9')
											);	
											$gab_query = new WP_Query();$gab_query->query($args); 
											while ($gab_query->have_posts()) : $gab_query->the_post();						
											if (of_get_option('of_dnd') == 1) { $do_not_duplicate[] = $post->ID; }
											?>
												<div class="item" id="post-<?php the_ID(); ?>">
													<?php
													gab_media(array(
														'name' => 'ac-midslider', 
														'imgtag' => 1,
														'link' => 1,
														'enable_video' => 1, 
														'catch_image' => of_get_option('of_catch_img', 0),
														'video_id' => 'midslider', 
														'enable_thumb' => 1,
														'resize_type' => 'c', /* c to crop, h to resize only height, w to resize only width */
														'media_width' => 479, 
														'media_height' => 265, 
														'thumb_align' => 'null',
														'enable_default' => 0
													)); 
													?>
													<div class="slidetext">
														<h2 class="posttitle">
															<a href="<?php the_permalink() ?>" rel="bookmark" title="<?php printf( esc_attr__( 'Permalink to %s', 'artcltr' ), the_title_attribute( 'echo=0' ) ); ?>" >
																<?php the_title(); ?>
															</a>
														</h2>
													</div>
												</div><!-- /item -->
											<?php 
											$count++; endwhile; wp_reset_query();
											?>
										</div><!-- /bottom-slider -->
										<div id="bottom_nav"></div>
									<?php }?>
									
									<?php gab_dynamic_sidebar('MidCol12'); ?>
		</div><!-- /midcol -->

		<div class="rightcol">
							<?php gab_dynamic_sidebar('RightCol1'); ?>
							
							<?php if (intval(of_get_option('of_ac_nr10', 1)) > 0 ) { ?>
							<!-- RIGHT COLUMN CATEGORY BLOCK 1 -->
							<div class="postcontainer">
								<?php
								echo '<p class="catname">';
									if(of_get_option('of_ac_capimg10') <> "" ) { 
										echo '<a href="' . get_category_link(of_get_option('of_ac_cat10')) . '">';
											echo '<img src="'.of_get_option('of_ac_capimg10').'" alt="" />';
											echo '</a>';
										} elseif ((of_get_option('of_cap10_1') <> '' ) or (of_get_option('of_cap10_2') <> '')) {
											echo '<span class="firstrow"><a href="'. get_category_link(of_get_option('of_ac_cat10')) .'">' . of_get_option('of_cap10_1') .'</a></span>';
											echo '<span class="secondrow"><a href="'. get_category_link(of_get_option('of_ac_cat10')) .'">' . of_get_option('of_cap10_2') . '</a></span>';
									} else {
										echo '<a href="'. get_category_link(of_get_option('of_ac_cat10')) .'">'. get_cat_name(of_get_option('of_ac_cat10')) .'</a>';
									}
								echo '</p>';
								
								$count = 1; 
								$args = array(
								  'posts_per_page' => of_get_option('of_ac_nr10'), 
								  'post__not_in'=> $do_not_duplicate,
								  'cat' => of_get_option('of_ac_cat10')
								);	
								$gab_query = new WP_Query();$gab_query->query($args); 
								while ($gab_query->have_posts()) : $gab_query->the_post();						
								if (of_get_option('of_dnd') == 1) { $do_not_duplicate[] = $post->ID; }
								?>
									<div class="featuredpost<?php if($count == of_get_option('of_ac_nr10')) { echo ' lastpost'; } ?>">
										<?php
										gab_media(array(
											'name' => 'ac-belowfeanarrow', 
											'imgtag' => 1,
											'link' => 1,
											'enable_video' => 1, 
											'catch_image' => of_get_option('of_catch_img', 0),
											'video_id' => 'belowfea6', 
											'enable_thumb' => 1,
											'resize_type' => 'c', /* c to crop, h to resize only height, w to resize only width */
											'media_width' => 217, 
											'media_height' => 140, 
											'thumb_align' => 'aligncenter',
											'enable_default' => 0
										)); 
										?>
												
										<h2 class="posttitle s_title">
											<a href="<?php the_permalink() ?>" rel="bookmark" title="<?php printf( esc_attr__( 'Permalink to %s', 'artcltr' ), the_title_attribute( 'echo=0' ) ); ?>" >
												<?php the_title(); ?>
											</a>
										</h2>

										<?php echo '<p>' . string_limit_words(get_the_excerpt(), 7) . '&hellip;</p>'; ?>							
									</div>
								<?php 
								$count++; endwhile; wp_reset_query();
								?>
							</div><!-- /postcontainer -->
							<?php } ?>
							
							<?php gab_dynamic_sidebar('RightCol2'); ?>
							
							<?php if (intval(of_get_option('of_ac_nr11', 1)) > 0 ) { ?>
							<!-- RIGHT COLUMN CATEGORY BLOCK 2 -->
							<div class="postcontainer">
								<?php
								echo '<p class="catname">';
									if(of_get_option('of_ac_capimg11') <> '' ) { 
										echo '<a href="' . get_category_link(of_get_option('of_ac_cat11')) . '">';
											echo '<img src="'.of_get_option('of_ac_capimg11').'" alt="" />';
											echo '</a>';
										} elseif ((of_get_option('of_cap11_1') <> '' ) or (of_get_option('of_cap11_2') <> '')) {
											echo '<span class="firstrow"><a href="'. get_category_link(of_get_option('of_ac_cat11')) .'">' . of_get_option('of_cap11_1') .'</a></span>';
											echo '<span class="secondrow"><a href="'. get_category_link(of_get_option('of_ac_cat11')) .'">' . of_get_option('of_cap11_2') . '</a></span>';
									} else {
										echo '<a href="'. get_category_link(of_get_option('of_ac_cat11')) .'">'. get_cat_name(of_get_option('of_ac_cat11')) .'</a>';
									}
								echo '</p>';
								
								$count = 1; 
								$args = array(
								  'posts_per_page' => of_get_option('of_ac_nr11'), 
								  'post__not_in'=> $do_not_duplicate,
								  'cat' => of_get_option('of_ac_cat11')
								);	
								$gab_query = new WP_Query();$gab_query->query($args); 
								while ($gab_query->have_posts()) : $gab_query->the_post();						
								if (of_get_option('of_dnd') == 1) { $do_not_duplicate[] = $post->ID; }
								?>
									<div class="featuredpost<?php if($count == of_get_option('of_ac_nr11')) { echo ' lastpost'; } ?>">
										<?php
										gab_media(array(
											'name' => 'ac-belowfeanarrow', 
											'imgtag' => 1,
											'link' => 1,
											'enable_video' => 1, 
											'catch_image' => of_get_option('of_catch_img', 0),
											'video_id' => 'belowfea7', 
											'enable_thumb' => 1,
											'resize_type' => 'c', /* c to crop, h to resize only height, w to resize only width */
											'media_width' => 217, 
											'media_height' => 140, 
											'thumb_align' => 'aligncenter',
											'enable_default' => 0
										)); 
										?>
												
										<h2 class="posttitle s_title">
											<a href="<?php the_permalink() ?>" rel="bookmark" title="<?php printf( esc_attr__( 'Permalink to %s', 'artcltr' ), the_title_attribute( 'echo=0' ) ); ?>" >
												<?php the_title(); ?>
											</a>
										</h2>

										<?php echo '<p>' . string_limit_words(get_the_excerpt(), 12) . '&hellip;</p>'; ?>							
									</div>
								<?php 
								$count++; endwhile; wp_reset_query();
								?>
							</div><!-- /postcontainer -->
							<?php } ?>
							
							<?php gab_dynamic_sidebar('RightCol3'); ?>
							
							<?php if (intval(of_get_option('of_ac_nr12', 1)) > 0 ) { ?>
							<!-- RIGHT COLUMN CATEGORY BLOCK 3 -->
							<div class="postcontainer">
								<?php
								echo '<p class="catname">';
									if(of_get_option('of_ac_capimg12') <> '' ) { 
										echo '<a href="' . get_category_link(of_get_option('of_ac_cat12')) . '">';
											echo '<img src="'.of_get_option('of_ac_capimg12').'" alt="" />';
											echo '</a>';
										} elseif ((of_get_option('of_cap12_1') <> '' ) or (of_get_option('of_cap12_2') <> '')) {
											echo '<span class="firstrow"><a href="'. get_category_link(of_get_option('of_ac_cat12')) .'">' . of_get_option('of_cap12_1') .'</a></span>';
											echo '<span class="secondrow"><a href="'. get_category_link(of_get_option('of_ac_cat12')) .'">' . of_get_option('of_cap12_2') . '</a></span>';
									} else {
										echo '<a href="'. get_category_link(of_get_option('of_ac_cat12')) .'">'. get_cat_name(of_get_option('of_ac_cat12')) .'</a>';
									}
								echo '</p>';
								
								$count = 1; 
								$args = array(
								  'posts_per_page' => of_get_option('of_ac_nr12'), 
								  'post__not_in'=> $do_not_duplicate,
								  'cat' => of_get_option('of_ac_cat12')
								);	
								$gab_query = new WP_Query();$gab_query->query($args); 
								while ($gab_query->have_posts()) : $gab_query->the_post();						
								if (of_get_option('of_dnd') == 1) { $do_not_duplicate[] = $post->ID; }
								?>
									<div class="featuredpost<?php if($count == of_get_option('of_ac_nr12')) { echo ' lastpost'; } ?>">
										<?php
										gab_media(array(
											'name' => 'ac-belowfeanarrow', 
											'imgtag' => 1,
											'link' => 1,
											'enable_video' => 1, 
											'catch_image' => of_get_option('of_catch_img', 0),
											'video_id' => 'belowfea8', 
											'enable_thumb' => 1,
											'resize_type' => 'c', /* c to crop, h to resize only height, w to resize only width */
											'media_width' => 217, 
											'media_height' => 140, 
											'thumb_align' => 'aligncenter',
											'enable_default' => 0
										)); 
										?>
												
										<h2 class="posttitle s_title">
											<a href="<?php the_permalink() ?>" rel="bookmark" title="<?php printf( esc_attr__( 'Permalink to %s', 'artcltr' ), the_title_attribute( 'echo=0' ) ); ?>" >
												<?php the_title(); ?>
											</a>
										</h2>

										<?php echo '<p>' . string_limit_words(get_the_excerpt(), 12) . '&hellip;</p>'; ?>						
									</div>
								<?php 
								$count++; endwhile; wp_reset_query();
								?>
							</div><!-- /postcontainer -->
							<?php } ?>
							
							<?php gab_dynamic_sidebar('RightCol4'); ?>
							
							<?php if (intval(of_get_option('of_ac_nr13', 1)) > 0 ) { ?>
							<!-- RIGHT COLUMN CATEGORY BLOCK 4 -->
							<div class="postcontainer">
								<?php
								echo '<p class="catname">';
									if(of_get_option('of_ac_capimg13') <> "" ) { 
										echo '<a href="' . get_category_link(of_get_option('of_ac_cat13')) . '">';
											echo '<img src="'.of_get_option('of_ac_capimg13').'" alt="" />';
											echo '</a>';
										} elseif ((of_get_option('of_cap13_1') <> '' ) or (of_get_option('of_cap13_2') <> '')) {
											echo '<span class="firstrow"><a href="'. get_category_link(of_get_option('of_ac_cat13')) .'"><span class="firstrow">' . of_get_option('of_cap13_1') .'</a></span>';
											echo '<span class="secondrow"><a href="'. get_category_link(of_get_option('of_ac_cat13')) .'">' . of_get_option('of_cap13_2') . '</a></span>';
									} else {
										echo '<a href="'. get_category_link(of_get_option('of_ac_cat13')) .'">'. get_cat_name(of_get_option('of_ac_cat13')) .'</a>';
									}
								echo '</p>';
								
								$count = 1; 
								$args = array(
								  'posts_per_page' => of_get_option('of_ac_nr13'), 
								  'post__not_in'=> $do_not_duplicate,
								  'cat' => of_get_option('of_ac_cat13')
								);	
								$gab_query = new WP_Query();$gab_query->query($args); 
								while ($gab_query->have_posts()) : $gab_query->the_post();						
								if (of_get_option('of_dnd') == 1) { $do_not_duplicate[] = $post->ID; }
								?>
										<?php
										if ($count == 1) {
										gab_media(array(
											'name' => 'ac-belowfeanarrow', 
											'imgtag' => 1,
											'link' => 1,
											'enable_video' => 1, 
											'catch_image' => of_get_option('of_catch_img', 0),
											'video_id' => 'belowfea9', 
											'enable_thumb' => 1,
											'resize_type' => 'c', /* c to crop, h to resize only height, w to resize only width */
											'media_width' => 217, 
											'media_height' => 140, 
											'thumb_align' => 'aligncenter',
											'enable_default' => 0
										)); 
										}
										?>
												
										<h2 class="posttitle list_title">
											<a href="<?php the_permalink() ?>" rel="bookmark" title="<?php printf( esc_attr__( 'Permalink to %s', 'artcltr' ), the_title_attribute( 'echo=0' ) ); ?>" >
												<?php gab_posttitle(32,'&hellip;'); ?>
											</a>
										</h2>

								<?php 
								$count++; endwhile; wp_reset_query();
								?>
							</div><!-- /postcontainer -->
							<?php } ?>
							
							<?php gab_dynamic_sidebar('RightCol5'); ?>
							
							<?php if (intval(of_get_option('of_ac_nr14', 1)) > 0 ) { ?>
							<!-- RIGHT COLUMN CATEGORY BLOCK 5 -->
							<div class="postcontainer last">
								<?php
								echo '<p class="catname">';
									if(of_get_option('of_ac_capimg14') <> "" ) { 
										echo '<a href="' . get_category_link(of_get_option('of_ac_cat14')) . '">';
											echo '<img src="'.of_get_option('of_ac_capimg14').'" alt="" />';
											echo '</a>';
										} elseif ((of_get_option('of_cap14_1') <> '' ) or (of_get_option('of_cap14_2') <> '')) {
											echo '<span class="firstrow"><a href="'. get_category_link(of_get_option('of_ac_cat14')) .'">' . of_get_option('of_cap14_1') .'</a></span>';
											echo '<span class="secondrow"><a href="'. get_category_link(of_get_option('of_ac_cat14')) .'">' . of_get_option('of_cap14_2') . '</a></span>';
									} else {
										echo '<a href="'. get_category_link(of_get_option('of_ac_cat14')) .'">'. get_cat_name(of_get_option('of_ac_cat14')) .'</a>';
									}
								echo '</p>';
								
								$count = 1; 
								$args = array(
								  'posts_per_page' => of_get_option('of_ac_nr14'), 
								  'post__not_in'=> $do_not_duplicate,
								  'cat' => of_get_option('of_ac_cat14')
								);	
								$gab_query = new WP_Query();$gab_query->query($args); 
								while ($gab_query->have_posts()) : $gab_query->the_post();						
								if (of_get_option('of_dnd') == 1) { $do_not_duplicate[] = $post->ID; }
								?>
										<?php
										if ($count == 1) {
										gab_media(array(
											'name' => 'ac-belowfeanarrow', 
											'imgtag' => 1,
											'link' => 1,
											'enable_video' => 1, 
											'catch_image' => of_get_option('of_catch_img', 0),
											'video_id' => 'belowfea10', 
											'enable_thumb' => 1,
											'resize_type' => 'c', /* c to crop, h to resize only height, w to resize only width */
											'media_width' => 217, 
											'media_height' => 140, 
											'thumb_align' => 'aligncenter',
											'enable_default' => 0
										)); 
										}
										?>
												
										<h2 class="posttitle list_title">
											<a href="<?php the_permalink() ?>" rel="bookmark" title="<?php printf( esc_attr__( 'Permalink to %s', 'artcltr' ), the_title_attribute( 'echo=0' ) ); ?>" >
												<?php gab_posttitle(32,'&hellip;'); ?>
											</a>
										</h2>
								<?php 
								$count++; endwhile; wp_reset_query();
								?>
							</div><!-- /postcontainer -->			
							<?php } ?>
							
							<?php gab_dynamic_sidebar('RightCol6'); ?>
		</div><!-- /rightcol -->
	</div><!-- /wrapper -->
</div><!-- /homecontent -->


<?php get_footer();?>