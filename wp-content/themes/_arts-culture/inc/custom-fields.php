<?php
	add_action( 'add_meta_boxes', 'gabfire_meta_box_add' );

	function gabfire_meta_box_add()
	{
		add_meta_box( '', 'Gabfire Custom Fields', 'gabfire_meta_box', 'post', 'normal', 'high' );
		add_meta_box( '', 'Gabfire Custom Fields', 'gabfire_meta_box', 'page', 'normal', 'high' );
		add_meta_box( '', 'Gabfire Custom Fields', 'gabfire_meta_box', 'gab_gallery', 'normal', 'high' );
		add_meta_box( '', 'Gabfire Custom Fields', 'gabfire_meta_box', 'testimonial', 'normal', 'high' );
		add_meta_box( '', 'Gabfire Custom Fields', 'gabfire_meta_box', 'quote', 'normal', 'high' );
		add_meta_box( '', 'Gabfire Custom Fields', 'gabfire_meta_box', 'service', 'normal', 'high' );
	}

	function gabfire_meta_box( $post )
	{
		$values = get_post_custom( $post->ID );
		$video = isset( $values['iframe'] ) ? esc_attr( $values['iframe'][0] ) : '';
		$artcltr_post = isset( $values['credit'] ) ? esc_attr( $values['credit'][0] ) : '';
		$feapost = isset( $values['featured'] ) ? esc_attr( $values['featured'][0] ) : '';  
		$s_title = isset( $values['title2'] ) ? esc_attr( $values['title2'][0] ) : '';  
		$selected = isset( $values['gabfire_post_template'] ) ? esc_attr( $values['gabfire_post_template'][0] ) : '';  
		wp_nonce_field( 'my_meta_box_nonce', 'meta_box_nonce' );
		?>		
			
		<p class="gab_caption">Video URL</p>
		<div class="gab_fieldrow">
			<div class="gab_fieldname"><label for="iframe"><?php _e('You can add any Youtube, Vimeo, Dailymotion or Screenr video url into this box','artcltr'); ?></label></div>
			<div class="gab_fieldinput"><input type="text" class="gab_textfield" name="iframe" class="regular-text" id="iframe" value="<?php echo $video; ?>" /></div>
			<div class="clear"></div>
		</div>
			
		<hr class="gab_hr">
			
		<p class="gab_caption">Credit / Source</p>
		<div class="gab_fieldrow">
			<div class="gab_fieldname"><label for="credit"><?php _e('Credit / artcltr of Post','artcltr'); ?></label></div>
			<div class="gab_fieldinput"><textarea class="gab_textfield" name="credit" class="regular-text" id="credit"><?php echo $artcltr_post; ?></textarea></div>
			<div class="clear"></div>
		</div>		
		
		<hr class="gab_hr">
		
		<p class="gab_caption">Is Featured?</p>
		<div class="gab_fieldrow">
			<div class="gab_fieldname"><label for="featured"><label for="featured"><?php _e('If featured post option set as custom fields on theme options page; check this box to display this post on featured section of front page','artcltr'); ?></label></div>
			<div class="gab_fieldinput"><input type="checkbox" id="featured" name="featured" <?php checked( $feapost, 'true' ); ?> />  </div>
			<div class="clear"></div>
		</div>	
		
		<hr class="gab_hr">
		
		<p class="gab_caption">Post Layout</p>
		<p>  
			<div class="gab_fieldname"><label for="gabfire_post_template">Select a Post layout</label></div>
			<div class="gab_fieldinput">
				<select name="gabfire_post_template" id="gabfire_post_template">  
					<option value="2col" <?php selected( $selected, '2col' ); ?>>2 Column</option>  
					<option value="3col" <?php selected( $selected, '3col' ); ?>>3 Column</option>  
					<option value="fullcol" <?php selected( $selected, 'fullcol' ); ?>>Full Width</option>  
				</select>
			</div>
			<div class="clear"></div>
		</p> 
		
		<p class="gab_caption">Title 2</p>
		<div class="gab_fieldrow">
			<div class="gab_fieldname"><label for="title2"><?php _e('If this is a post displayed on right hand featured slider; enter a text to display as second title','artcltr'); ?></label></div>
			<div class="gab_fieldinput"><input type="text" class="gab_textfield" name="title2" class="regular-text" id="title2" value="<?php echo $s_title; ?>" /></div>
			<div class="clear"></div>
		</div>		
				
		<?php	
	}

	add_action( 'save_post', 'gabfire_meta_box_save' );
	function gabfire_meta_box_save( $post_id )
	{
		// Bail if we're doing an auto save
		if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) return;
		
		// if our nonce isn't there, or we can't verify it, bail
		if( !isset( $_POST['meta_box_nonce'] ) || !wp_verify_nonce( $_POST['meta_box_nonce'], 'my_meta_box_nonce' ) ) return;
		
		// if our current user can't edit this post, bail
		if( !current_user_can( 'edit_post' ) ) return;
		
		// now we can actually save the data
		$allowed = array( 
			'a' => array( // on allow a tags
				'href' => array() // and those anchords can only have href attribute
			)
		);
		
		// Probably a good idea to make sure your data is set
		if( isset( $_POST['iframe'] ) && !empty( $_POST['iframe'] ) )
			update_post_meta( $post_id, 'iframe', wp_kses( $_POST['iframe'], $allowed ) );
			
		if( isset( $_POST['title2'] ) && !empty( $_POST['title2'] ) )
			update_post_meta( $post_id, 'title2', wp_kses( $_POST['title2'], $allowed ) );
			
		if( isset( $_POST['credit'] ) && !empty( $_POST['credit'] ) )
			update_post_meta( $post_id, 'credit', wp_kses( $_POST['credit'], $allowed ) );			
			
		if( isset( $_POST['gabfire_post_template'] ) && !empty( $_POST['gabfire_post_template'] ) )	
			update_post_meta( $post_id, 'gabfire_post_template', esc_attr( $_POST['gabfire_post_template'] ) );  				

		// This is purely my personal preference for saving check-boxes  
		$chk = isset( $_POST['featured'] ) && $_POST['featured'] ? 'true' : 'false';  
		update_post_meta( $post_id, 'featured', $chk );  
	
	}
	
	if (is_admin()) {	
		add_action('admin_print_styles-post.php', 'gabfire_adminstyle');
		add_action('admin_print_styles-post-new.php', 'gabfire_adminstyle');

		function gabfire_adminstyle() {
			wp_enqueue_style('adminstyle', get_template_directory_uri() .'/inc/custom-fields.css');
		}
	}