<?php
set_post_thumbnail_size( 50, 50, true); /* Normal Post Thumbnails */

if (of_get_option('of_wpmumode')==0) {
	add_image_size( 'gabfire', 1024, 9999 ); /* Featured Big Image - defined to be used with timthumb */
} else {
	/* Theme thumbnail sizes for WordPress multi user
	 * network installations. The image sizes below will  
	 * be used only when timthumb is disabled on 
	 * theme options -> under image handling settings tab
	 */
	add_image_size( 'ac-featured', 332, 212, true ); /* Featured */
	add_image_size( 'ac-feathumb', 82, 54, true ); /* Featured thumbnail */
	add_image_size( 'ac-belowfeanarrow', 217, 140, true ); /* Homepage, below slider left and right column */
	add_image_size( 'ac-belowfeamid1', 479, 300, true ); /*  Homepage, below slider mid column, top big image */
	add_image_size( 'ac-belowfeamid2', 227, 120, true ); /*  Homepage, below slider mid column, 2column's wide image */
	add_image_size( 'ac-belowfeamid3', 80, 51, true ); /*  Homepage, below slider mid column, 2column's narrow image */
	add_image_size( 'ac-midslider', 479, 265, true ); /*  Homepage, below slider mid column, slider image */
	add_image_size( 'ac-default_ajaxtabs', 80, 51, true ); /*  Sidebar, default tabs' thumbs */
	add_image_size( 'ajaxtabs', 30, 30, true ); // Ajaxtabs Widget
	add_image_size( 'ac-archive_big', 960, 450, true ); /* Category template with Slider - big thumb */
	add_image_size( 'ac-loop-default', 630, 300, true ); /* Default Loop Category Archive thumbnail */
	add_image_size( 'ac-mag1', 463, 310, true ); /* Magazine style category template thumb */
	add_image_size( 'ac-mag2', 479, 300, true ); /* Magazine style category template thumb */
	add_image_size( 'ac-mag3', 227, 120, true ); /* Magazine style category template thumb */
	add_image_size( 'ac-mag4', 479, 265, true ); /* Magazine style category template thumb */
	add_image_size( 'ac-loop4col', 220, 135, true ); /* 4 column category Archive thumbnail */
	add_image_size( 'ac-2colloop', 304, 220, true ); /* 2 column category Archive thumbnail */
	add_image_size( 'ac-medialoop', 310, 230, true );/* Media category archive thumbnail */
	add_image_size( 'ac-medialoop-overlay', 600, 9999 ); /* Media category archive - overlay thumbnail */
	add_image_size( 'ac-fullwidth', 970, 9999 ); /* full width post template image size */
	add_image_size( 'ac-3colpost', 468, 9999 ); /* 3 column post template image size */
}