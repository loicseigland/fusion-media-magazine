<?php 
if ( ! function_exists( 'gab_comment' ) ) :
/**
 * Template for comments and pingbacks.
 *
 * To override this walker in a child theme without modifying the comments template
 * simply create your own gab_comment(), and that function will be used instead.
 *
 * Used as a callback by wp_list_comments() for displaying the comments.
 *
 */
function gab_comment( $comment, $args, $depth ) {
	$GLOBALS['comment'] = $comment;
	switch ( $comment->comment_type ) :
		case '' :
	?>
	<li <?php comment_class(); ?> id="li-comment-<?php comment_ID(); ?>">

		<div class="comment-inner" id="comment-<?php comment_ID(); ?>">
		
			<div class="comment-top">
				<div class="comment-avatar">
					<?php echo get_avatar( $comment, 40 ); ?>
				</div> 
				
				<div class="commentmeta">
					<?php printf( __( '%s ', 'artcltr'), sprintf( '<cite class="fn">%s</cite>', get_comment_author_link() ) ); ?>
					<?php echo get_comment_date(); ?> <?php _e( 'at', 'artcltr'); ?> <?php echo get_comment_time(); ?>
					<?php edit_comment_link( __( 'Edit', 'artcltr'), '(' , ')'); ?>				
				</div>
			</div>				
			
			<?php if ( $comment->comment_approved == '0' ) : ?>
				<p class="waiting_approval"><?php _e( 'Your comment is awaiting moderation.', 'artcltr' ); ?></p>
			<?php endif; ?>
			
			<?php comment_text(); ?>
			
			<span class="reply"><?php comment_reply_link( array_merge( $args, array( 'depth' => $depth, 'max_depth' => $args['max_depth'] ) ) ); ?></span>
			
		</div><!-- comment-inner  -->

	<?php
			break;
		case 'pingback'  :
		case 'trackback' :
	?>
	<li class="post pingback">
		<p><?php _e( 'Pingback:', 'artcltr' ); ?> <?php comment_author_link(); ?><?php edit_comment_link( __( 'Edit', 'artcltr' ), ' ' ); ?></p>
	<?php
			break;
	endswitch;
}
endif;