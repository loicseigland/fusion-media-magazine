<?php
/* Create Custom Post Type */
add_action('init', 'gallery_init');
function gallery_init() 
{
  $labels = array(
  
	'name' => __( 'Media Gallery', 'artcltr' ),
	'singular_name' => __( 'Multimedia', 'artcltr' ),
	'add_new' => __( 'Add New', 'artcltr' ),
	'add_new_item' => __( 'Add New', 'artcltr' ),
	'edit_item' => __( 'Edit', 'artcltr' ),
	'new_item' => __( 'New', 'artcltr' ),
	'view_item' => __( 'View', 'artcltr' ),
	'search_items' => __( 'Search', 'artcltr' ),
	'not_found' => __( 'Nothing found', 'artcltr' ),
	'not_found_in_trash' => __( 'Nothing found in Trash', 'artcltr' ),
	'parent_item_colon' => __( 'Parent:', 'artcltr' ),
	'menu_name' => __( 'Multimedia', 'artcltr' ), 
  );
    
  $args = array(
        'labels' => $labels,
        'hierarchical' => true,
		'menu_icon' => get_template_directory_uri() . '/framework/images/gabfire-icon.png',
        'supports' => array( 'title', 'editor', 'comments', 'author', 'excerpt', 'thumbnail', 'custom-fields', 'revisions', 'post-formats' ),
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'show_in_nav_menus' => false,
        'publicly_queryable' => true,
        'exclude_from_search' => false,
        'has_archive' => true,
        'query_var' => true,
        'can__xport' => true,
        'rewrite' => true,
        'capability_type' => 'post'
  ); 
  register_post_type('gab_gallery',$args);
}

//hook into the init action and call create_book_taxonomies when it fires
add_action( 'init', 'gallery_taxonomies', 0 );

//create two taxonomies, genres and writers for the post type "book"
function gallery_taxonomies() 
{
  // Add new taxonomy, make it hierarchical (like categories)
  $labels = array(
    'name' => __( 'gallery-cats', 'artcltr' ),
    'singular_name' => __( 'gallery-cat', 'artcltr' ),
    'search_items' =>  __( 'Search', 'artcltr' ),
    'all_items' => __( 'All', 'artcltr' ),
    'parent_item' => __( 'Parent', 'artcltr' ),
    'parent_item_colon' => __( 'Parent:', 'artcltr' ),
    'edit_item' => __( 'Edit', 'artcltr' ), 
    'update_item' => __( 'Update', 'artcltr' ),
    'add_new_item' => __( 'Add New', 'artcltr' ),
    'new_item_name' => __( 'New Name', 'artcltr' ),
    'menu_name' => __( 'Gallery Categories', 'artcltr' ),
  ); 	

  register_taxonomy('gallery-cat',array('gab_gallery'), array(
    'hierarchical' => true,
    'labels' => $labels,
    'show_ui' => true,
    'query_var' => true,
    'rewrite' => array( 'slug' => 'gallery-cats' ),
  ));

  // Add new taxonomy, NOT hierarchical (like tags)
  $labels = array(
    'name' => __( 'gallery-tags', 'artcltr' ),
    'singular_name' => __( 'gallery-tag', 'artcltr'),
    'search_items' =>  __( 'Search', 'artcltr' ),
    'popular_items' => __( 'Popular', 'artcltr' ),
    'all_items' => __( 'All', 'artcltr' ),
    'parent_item' => null,
    'parent_item_colon' => null,
    'edit_item' => __( 'Edit', 'artcltr' ), 
    'update_item' => __( 'Update', 'artcltr' ),
    'add_new_item' => __( 'Add New', 'artcltr' ),
    'new_item_name' => __( 'New', 'artcltr' ),
    'separate_items_with_commas' => __( 'Separate with commas', 'artcltr' ),
    'add_or_remove_items' => __( 'Add or remove', 'artcltr' ),
    'choose_from_most_used' => __( 'Choose from the most used', 'artcltr' ),
    'menu_name' => __( 'Gallery Tags', 'artcltr' ),
  ); 

  register_taxonomy('gallery-tag','gab_gallery',array(
    'hierarchical' => false,
    'labels' => $labels,
    'show_ui' => true,
    'query_var' => true,
    'rewrite' => array( 'slug' => 'gallery-tag' ),
  ));
}