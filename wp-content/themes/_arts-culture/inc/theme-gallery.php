<?php
$number_photos = -1; 		// -1 to display all
$photo_size = 'large';		// The standard WordPress size to use for the large image
$thumb_size = 'thumbnail';	// The standard WordPress size to use for the thumbnail
$photo_width = 960;		// Width of photo
$photo_height = 450;		// height of photo
$themeurl = get_template_directory_uri();

$attachments = get_children( array(
'post_parent' => $post->ID,
'numberposts' => $number_photos,
'post_type' => 'attachment',
'post_mime_type' => 'image',
'order' => 'ASC', 
'orderby' => 'menu_order')
);

if ( !empty($attachments) ) :
	$counter = 0;
	$photo_output = '';
	$thumb_output = '';	
	foreach ( $attachments as $att_id => $attachment ) {
		$counter++;
		
		# Caption
		$image_meta = "";
		if ($attachment->post_excerpt) {
			$caption = $attachment->post_excerpt;
			$image_meta = "title='$caption' alt='$caption'";
		} else {
			$image_meta = "alt=''";
		}
	
		# Large photo
		$src = wp_get_attachment_image_src($attachment->ID, 'ac-archive_big', true);
		$full = wp_get_attachment_image_src($attachment->ID, 'large', true);
		
		if (of_get_option('of_wpmumode')==0) {
			if(is_multisite()) { 
				$photo_output .= '<li><img style="width:'.$photo_width.'px;height:auto;display:block;" src="'.esc_url($themeurl).'/timthumb.php?src='.urlencode(redirect_wpmu($full[0])).'&amp;q=90&amp;w='.$photo_width.'&amp;zc=1" class="" alt="'.$image_meta.' title="'.$image_meta.' /></li>';				
			} else {
				$photo_output .= '<li><img style="width:'.$photo_width.'px;height:auto;display:block;" src="'.esc_url($themeurl).'/timthumb.php?src='.urlencode($full[0]).'&amp;q=90&amp;w='.$photo_width.'&amp;zc=1" class="" alt="'.$image_meta.' title="'.$image_meta.' /></li>';		
			}		
		} else {
				$photo_output .= '<li><img style="width:'.$photo_width.'px;height:auto;display:block;" src="'. $src[0] .'" class="" alt="'.$image_meta.' title="'.$image_meta.' /></li>';
		}
	}  
endif; ?>

<?php if ($counter > 1) : ?>
<div class="wrapper">
	<div class="flexslider">
		<ul class="bxslider bxarchiveslider">
			<?php echo $photo_output; ?>
		</ul>
	</div>
</div>
<div class="clear"></div>
<?php endif; ?>
