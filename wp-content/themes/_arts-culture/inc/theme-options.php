<?php
/**
 * A unique identifier is defined to store the options in the database and reference them from the theme.
 * By default it uses the theme name, in lowercase and without spaces, but this can be changed if needed.
 * If the identifier changes, it'll appear as if the options have been reset.
 * 
 */
function optionsframework_option_name() {

	// This gets the theme name from the stylesheet
	$themename = wp_get_theme();
	$themename = $themename->{'Name'};
	$themename = preg_replace("/\W/", "_", strtolower($themename) );

	$optionsframework_settings = get_option( 'optionsframework' );
	$optionsframework_settings['id'] = $themename;
	update_option( 'optionsframework', $optionsframework_settings );
}

/**
 * Defines an array of options that will be used to generate the settings page and be saved in the database.
 * When creating the "id" fields, make sure to use all lowercase and no spaces.
 *  
 */

function optionsframework_options() {

	// VARIABLES
	$GLOBALS['template_path'] = OF_DIRECTORY;
	$themename = wp_get_theme();
	$themename = $themename->{'Name'};
	$shortname = "of";
	$themeid = "_ac";
	
	// Image Alignment radio box
	$options_thumb_align = array("alignleft" => "Left","alignright" => "Right","aligncenter" => "Center"); 

	// Image Links to Options
	$options_image_link_to = array("image" => "The Image","post" => "The Post"); 

	//Default Arrays 
	$options_revealtype = array("OnMouseover", "OnClick");
	$options_nr = array(0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20);
	$options_inslider = array("Disable", "Tag-based", "Site Wide");
	$options_sort = array("ASC" => "asc", "desc" => "desc");
	$options_order = array("id" => "id", "name" => "name", "count" => "count");
	$options_logo = array("text" => "Text Based Logo", "image" => "Image Based Logo");
	$options_feaslide = array("scrollUp", "scrollDown", "scrollLeft", "scrollRight", "turnUp", "turnDown", "turnLeft", "turnRight", "fade");

	//Stylesheets Reader
	$alt_stylesheet_path = OF_FILEPATH . "/styles/";
	$alt_stylesheets = array();
	
	// Pull all the categories into an array
	$options_categories = array(); 
	$options_categories_obj = get_categories("hide_empty=0");
	foreach ($options_categories_obj as $category) {
 	$options_categories[$category->cat_ID] = $category->cat_name;
	}
	
	//Access the WordPress Pages via an Array
	$options_pages = array();
	$options_pages_obj = get_pages("sort_column=post_parent,menu_order"); 
	foreach ($options_pages_obj as $of_page) {
		$options_pages[$of_page->ID] = $of_page->post_name; }
	$options_pages_tmp = array_unshift($options_pages, "Select a page:");
	
	if ( is_dir($alt_stylesheet_path) ) {
		if ($alt_stylesheet_dir = opendir($alt_stylesheet_path) ) { 
			while ( ($alt_stylesheet_file = readdir($alt_stylesheet_dir)) !== false ) {
				if(stristr($alt_stylesheet_file, ".css") !== false) {
					$alt_stylesheets[] = $alt_stylesheet_file;
				}
			} 
		}
	}

	//More Options
	$uploads_arr = wp_upload_dir();
	$all_uploads_path = $uploads_arr["path"];
	$all_uploads = get_option("of_uploads");
	$other_entries = array("Select a number:",1,"2","3",4,"5","6","7","8","9","10","11","12","13","14","15","16","17","18","19");
	$body_repeat = array("no-repeat","repeat-x","repeat-y","repeat");
	$body_pos = array("top left","top center","top right","center left","center center","center right","bottom left","bottom center","bottom right");
	
	// Pull all the pages into an array
	$options_pages = array(); 
	$options_pages_obj = get_pages("sort_column=post_parent,menu_order");
	$options_pages[""] = "Select a page:";
	foreach ($options_pages_obj as $page) {
 	$options_pages[$page->ID] = $page->post_title;
	}
		
	// If using image radio buttons, define a directory path
	$imagepath = get_stylesheet_directory_uri() . "/images/";
		
	$options = array();
		
	$options[] = array( "name" => "General Settings", "type" => "heading");
					
		$options[] = array( "name" => "Theme Stylesheet",
							"desc" => "Select your themes color scheme (i.e., active style).",
							"id" => $shortname."_alt_stylesheet",
							"std" => "default.css",
							"type" => "select",
							"options" => $alt_stylesheets);
	
		$url = get_stylesheet_directory_uri()."/framework/admin/images/";
		$options[] = array( "name" => "Select header style",
							"desc" => "Set your header preference based on the layouts options shown.",
							"id" => $shortname."_header_type",
							"type" => "images",
							"options" => array(
								"logobanner" => $url . "logobanner.gif",
								"singleimage" => $url . "single-image.gif",
								"adlogoad" => $url . "adlogoad.gif",
								"logosearch" => $url . "logo-search.gif",
								));

		$options[] = array( "name" => "Header image (if single image selected)",
							"desc" => "If single image option is selected as header type, upload a header banner that is set to 980px width.",
							"id" => $shortname."_himageurl",
							"type" => "upload");

		$options[] = array( "name" => "Logo Type",
							"desc" => "Choose between an image-based logo - or - a text-based logo.",
							"id" => $shortname."_logo_type",
							"std" => "Text Based Logo",
							"type" => "select",
							"options" => $options_logo); 

		$options[] = array( "name" => "Custom Logo",
							"desc" => "If image-based logo is selected, upload a logo for your theme, or specify the image address of your online logo. (http://yoursite.com/logo.png) [Max Width: 360px]",
							"id" => $shortname."_logo",
							"type" => "upload");
							
		$options[] = array( "name" => "Text Logo",
							"desc" => "If text-based logo is selected, enter text to display on first row.",
							"id" => $shortname."_logo1",
							"type" => "text");
							
		$options[] = array( "desc" => "If text-based logo is selected, enter text to display on second row.",
							"id" => $shortname."_logo2",
							"type" => "text");
							
		$options[] = array( "name" => "Logo Padding Top",
							"desc" => "Set a padding value between logo and top line.",
							"id" => $shortname."_padding_top",
							"std" => 25,
							"class" => "mini",
							"type" => "text");

		$options[] = array( "name" => "Logo Padding Bottom",
							"desc" => "Set a padding value between logo and bottom line.",
							"id" => $shortname."_padding_bottom",
							"std" => 25,
							"class" => "mini",
							"type" => "text");
							
		$options[] = array( "name" => "Logo Padding Left",
							"desc" => "Set a padding value between logo and left line.",
							"id" => $shortname."_padding_left",
							"std" => 0,
							"type" => "text");
							
		$options[] = array( "name" => "Searchbox Padding Top (if header type is selected as Logo + Search)",
							"desc" => "Set a padding value between logo and top line.",
							"id" => $shortname."_searchmrgn",
							"std" => 45,
							"class" => "mini",
							"type" => "text");							
							
		$options[] = array( "name" => "Custom Favicon",
							"desc" => "Upload a 16px x 16px Png/Gif image that will represent your website\"s favicon.",
							"id" => $shortname."_custom_favicon",
							"type" => "upload"); 
							
		$options[] = array( "name" => "RSS",
							"desc" => "Link to third party feed handler. <br/> [http://www.url.com]",
							"id" => $shortname."_rssaddr",
							"type" => "text"); 				
		
		$options[] = array( "name" => "Tracking Code",
							"desc" => "Paste your Google Analytics (or other) tracking code here. This will be added into the footer template of your theme.",
							"id" => $shortname."_google_analytics",
							"type" => "textarea"); 						

	$options[] = array( "name" => "Navigation", "type" => "heading");			

		$options[] = array( "desc" => "Replace primary navigation with a custom menu. [If selected, create a <a href=\"nav-menus.php\" target=\"_blank\">custom menu</a>]",
							"id" => $shortname."_nav1",
							"std" => 0,
							"type" => "checkbox");
							
		$options[] = array( "desc" => "Replace secondary navigation with a custom menu. [If selected, create a <a href=\"nav-menus.php\" target=\"_blank\">custom menu</a>]",
							"id" => $shortname."_nav2",
							"std" => 0,
							"type" => "checkbox");
							
		$options[] = array( "desc" => "Replace footer navigation with a custom menu. [If selected, create a <a href=\"nav-menus.php\" target=\"_blank\">custom menu</a>]",
							"id" => $shortname."_nav3",
							"std" => 0,
							"type" => "checkbox");
													
		$options[] = array( "name" => "Sort Categories",
							"desc" => "Display categories in ascending or descending order",
							"id" => $shortname."_sort_cats",
							"type" => "select",
							"class" => "mini",
							"options" => $options_sort);

		$options[] = array( "name" => "Order Categories",
							"desc" => "Display categories in alphabetical order, by category ID, or by the count of posts",
							"id" => $shortname."_order_cats",
							"std" => "name",
							"type" => "select",
							"class" => "mini",
							"options" => $options_order);
							
		$options[] = array( "name" => "Exclude Categories",
							"desc" => "ID number of cat(s) to exclude from navigation (eg 1,2,3,4) <a href=\"http://www.gabfirethemes.com/how-to-check-category-ids/\" target=\"_blank\">How check category/page ID</a>",
							"id" => $shortname."_ex_cats",
							"class" => "mini",
							"type" => "text"); 
							
		$options[] = array( "name" => "Exclude Pages - Footer",
							"desc" => "ID number of page(s) to exclude from footer navigation (eg 1,2,3,4) <a href=\"http://www.gabfirethemes.com/how-to-check-category-ids/\" target=\"_blank\">How check category/page ID</a>",
							"id" => $shortname."_ex_pagesfot",
							"class" => "mini",
							"type" => "text");							

	$options[] = array( "name" => "Default Widgets", "type" => "heading");	
			
		$options[] = array( "name" => "Enable Tabbed Content ",
						"desc" => "Enable Popular and Recent news tabbed content on sidebar",
						"id" => $shortname.$themeid."_tabcontent",
						"type" => "checkbox"); 
						
		$options[] = array( "desc" => "Label for Popular News tab.",
							"id" => $shortname.$themeid."_poplabel",
							"std" => 'POPULAR NEWS',
							"class" => "mini",
							"type" => "text"); 						
							
		$options[] = array( "desc" => "Number of Entries to display for Popular news",
							"id" => $shortname.$themeid."_popularnr",
							"std" => 4,
							"class" => "mini",
							"type" => "select",
							"options" => $options_nr); 
							
		$options[] = array( "desc" => "Label for Recent News tab.",
							"id" => $shortname.$themeid."_reclabel",
							"std" => 'RECENT NEWS',
							"class" => "mini",
							"type" => "text"); 								
							
		$options[] = array( "desc" => "Number of Entries to display for Recent news",
							"id" => $shortname.$themeid."_recentnr",
							"std" => 4,
							"class" => "mini",
							"type" => "select",
							"options" => $options_nr); 
		
	$options[] = array( "name" => "Categories", "type" => "heading");
		$options[] = array( "name" => "Do not duplicate posts",
							"desc" => "Regardless of category selections, do not duplicate posts on homepage. This prevents the same post from appearing in multiple sections (good for SEO)",
							"id" => $shortname."_dnd",
							"std" => 0,
							"type" => "checkbox");	
							
		$options[] = array( "name" => "Featured Slider",
							"desc" => "Select a category to use for featured area.",
							"id" => $shortname.$themeid."_fea_cat",
							"type" => "select",
							"options" => $options_categories);				
							
		$options[] = array( "desc" => "Number of Entries to display. (Leave as 6)",
							"id" => $shortname.$themeid."_nrfea",
							"std" => 6,
							"class" => "mini",
							"type" => "select",
							"options" => $options_nr); 
														
		$options[] = array( "desc" => "Display posts assigned this tag to be listed on featured slider. <br/> [Note: Category will be disregarded if a tag name is filled in].",
							"id" => $shortname.$themeid."_fea_tag",
							"class" => "mini",
							"type" => "text"); 
							
		$options[] = array( "desc" => "If this box is checked and featured tag section left empty, entries with custom field name <strong>featured</strong> and custom field value <strong>true</strong> will be displayed on slider.",
							"id" => $shortname.$themeid."_fea_cf",
							"std" => 0,
							"type" => "checkbox");	

		$options[] = array( "desc" => "Display most recent posts in featured area instead of category or tag.",
							"id" => $shortname.$themeid."_fea_recent",
							"std" => 0,
							"type" => "checkbox");	
							
		$options[] = array( "name" => "Right hand of Featured Slider",
							"desc" => "Select a category to use.",
							"id" => $shortname.$themeid."_feacat2",
							"type" => "select",
							"options" => $options_categories);				
							
		$options[] = array( "desc" => "Number of Entries to display",
							"id" => $shortname.$themeid."_feanr2",
							"std" => 4,
							"class" => "mini",
							"type" => "select",
							"options" => $options_nr); 

		$options[] = array( "name" => "Home - Left Col, 1st Cat",
							"desc" => "Select a category to use.",
							"id" => $shortname.$themeid."_cat1",
							"type" => "select",
							"options" => $options_categories);				
							
		$options[] = array( "desc" => "Number of Entries to display",
							"id" => $shortname.$themeid."_nr1",
							"std" => 2,
							"class" => "mini",
							"type" => "select",
							"options" => $options_nr); 
							
		$options[] = array( "name" => "Home - Left Col, 2nd Cat",
							"desc" => "Select a category to use.",
							"id" => $shortname.$themeid."_cat2",
							"type" => "select",
							"options" => $options_categories);				
							
		$options[] = array( "desc" => "Number of Entries to display",
							"id" => $shortname.$themeid."_nr2",
							"std" => 1,
							"class" => "mini",
							"type" => "select",
							"options" => $options_nr); 
														
		$options[] = array( "name" => "Home - Left Col, 3rd Cat",
							"desc" => "Select a category to use.",
							"id" => $shortname.$themeid."_cat3",
							"type" => "select",
							"options" => $options_categories);				
							
		$options[] = array( "desc" => "Number of Entries to display",
							"id" => $shortname.$themeid."_nr3",
							"std" => 1,
							"class" => "mini",
							"type" => "select",
							"options" => $options_nr); 
							
		$options[] = array( "name" => "Home - Left Col, 4th Cat",
							"desc" => "Select a category to use.",
							"id" => $shortname.$themeid."_catn1",
							"type" => "select",
							"options" => $options_categories);				
							
		$options[] = array( "desc" => "Number of Entries to display",
							"id" => $shortname.$themeid."_nrn1",
							"std" => 3,
							"class" => "mini",
							"type" => "select",
							"options" => $options_nr); 
							
		$options[] = array( "name" => "Home - Left Col, 5th Cat",
							"desc" => "Select a category to use.",
							"id" => $shortname.$themeid."_catn2",
							"type" => "select",
							"options" => $options_categories);				
							
		$options[] = array( "desc" => "Number of Entries to display",
							"id" => $shortname.$themeid."_nrn2",
							"std" => 3,
							"class" => "mini",
							"type" => "select",
							"options" => $options_nr); 
							
		$options[] = array( "name" => "Home - Mid Col, Top Wide Cat",
							"desc" => "Select a category to use.",
							"id" => $shortname.$themeid."_cat4",
							"type" => "select",
							"options" => $options_categories);				
							
		$options[] = array( "desc" => "Number of Entries to display",
							"id" => $shortname.$themeid."_nr4",
							"std" => 1,
							"class" => "mini",
							"type" => "select",
							"options" => $options_nr); 
							
		$options[] = array(	"desc" => "Display a custom text above mid column below featured slider on homepage",
							"id" => $shortname.$themeid."_midslogan",
							"type" => "textarea"); 		

		$options[] = array( "name" => "Home - Mid Col, Top left of 4 vertical cols",
							"desc" => "Select a category to use.",
							"id" => $shortname.$themeid."_cat5",
							"type" => "select",
							"options" => $options_categories);				
							
		$options[] = array( "desc" => "Number of Entries to display",
							"id" => $shortname.$themeid."_nr5",
							"std" => 3,
							"class" => "mini",
							"type" => "select",
							"options" => $options_nr); 					
							
		$options[] = array( "name" => "Home - Mid Col, Top right of 4 vertical cols",
							"desc" => "Select a category to use.",
							"id" => $shortname.$themeid."_cat6",
							"type" => "select",
							"options" => $options_categories);				
							
		$options[] = array( "desc" => "Number of Entries to display",
							"id" => $shortname.$themeid."_nr6",
							"std" => 3,
							"class" => "mini",
							"type" => "select",
							"options" => $options_nr); 
														
		$options[] = array( "name" => "Home -  Mid Col, Bottom left of 4 vertical cols",
							"desc" => "Select a category to use.",
							"id" => $shortname.$themeid."_cat7",
							"type" => "select",
							"options" => $options_categories);				
							
		$options[] = array( "desc" => "Number of Entries to display",
							"id" => $shortname.$themeid."_nr7",
							"std" => 1,
							"class" => "mini",
							"type" => "select",
							"options" => $options_nr); 
														
		$options[] = array( "name" => "Home - Mid Col, Bottom right of 4 vertical cols",
							"desc" => "Select a category to use.",
							"id" => $shortname.$themeid."_cat8",
							"type" => "select",
							"options" => $options_categories);				
							
		$options[] = array( "desc" => "Number of Entries to display",
							"id" => $shortname.$themeid."_nr8",
							"std" => 1,
							"class" => "mini",
							"type" => "select",
							"options" => $options_nr); 
														
		$options[] = array( "name" => "Home - Mid Col, bottom Wide Cat",
							"desc" => "Select a category to use.",
							"id" => $shortname.$themeid."_cat9",
							"type" => "select",
							"options" => $options_categories);				
							
		$options[] = array( "desc" => "Number of Entries to display",
							"id" => $shortname.$themeid."_nr9",
							"std" => 4,
							"class" => "mini",
							"type" => "select",
							"options" => $options_nr);
							
		$options[] = array( "name" => "Home - Right Col, 1st Cat",
							"desc" => "Select a category to use.",
							"id" => $shortname.$themeid."_cat10",
							"type" => "select",
							"options" => $options_categories);				
							
		$options[] = array( "desc" => "Number of Entries to display",
							"id" => $shortname.$themeid."_nr10",
							"std" => 1,
							"class" => "mini",
							"type" => "select",
							"options" => $options_nr);	
			
		$options[] = array( "name" => "Home - Right Col, 2nd Cat",
							"desc" => "Select a category to use.",
							"id" => $shortname.$themeid."_cat11",
							"type" => "select",
							"options" => $options_categories);				
							
		$options[] = array( "desc" => "Number of Entries to display",
							"id" => $shortname.$themeid."_nr11",
							"std" => 1,
							"class" => "mini",
							"type" => "select",
							"options" => $options_nr);		
			
		$options[] = array( "name" => "Home - Right Col, 3rd Cat",
							"desc" => "Select a category to use.",
							"id" => $shortname.$themeid."_cat12",
							"type" => "select",
							"options" => $options_categories);				
							
		$options[] = array( "desc" => "Number of Entries to display",
							"id" => $shortname.$themeid."_nr12",
							"std" => 1,
							"class" => "mini",
							"type" => "select",
							"options" => $options_nr);						
					
		$options[] = array( "name" => "Home - Right Col, 4th Cat",
							"desc" => "Select a category to use.",
							"id" => $shortname.$themeid."_cat13",
							"type" => "select",
							"options" => $options_categories);				
							
		$options[] = array( "desc" => "Number of Entries to display",
							"id" => $shortname.$themeid."_nr13",
							"std" => 3,
							"class" => "mini",
							"type" => "select",
							"options" => $options_nr);	
							
		$options[] = array( "name" => "Home - Right Col, 5th Cat",
							"desc" => "Select a category to use.",
							"id" => $shortname.$themeid."_cat14",
							"type" => "select",
							"options" => $options_categories);				
							
		$options[] = array( "desc" => "Number of Entries to display",
							"id" => $shortname.$themeid."_nr14",
							"std" => 3,
							"class" => "mini",
							"type" => "select",
							"options" => $options_nr);	
							
	$options[] = array( "name" => "Category Captions", "type" => "heading");	
	
		$options[] = array( "name" => "Home - Left Col, 1st Cat",
							"desc" => "Upload an image headline for this block",
							"id" => $shortname.$themeid."_capimg1",
							"type" => "upload");

		$options[] = array( 'desc' => 'If image left empty -> Category caption 1st row',
							'id' => $shortname.'_cap1_1',
							'class' => 'mini',
							'type' => 'text');
							
		$options[] = array( 'desc' => 'If image left empty -> Category caption 2nd row',
							'id' => $shortname.'_cap1_2',
							'class' => 'mini',
							'type' => 'text');
							
		$options[] = array( "name" => "Home - Left Col, 2nd Cat",
							"desc" => "Upload an image headline for this block",
							"id" => $shortname.$themeid."_capimg2",
							"type" => "upload");
							
		$options[] = array( 'desc' => 'If image left empty -> Category caption 1st row',
							'id' => $shortname.'_cap2_1',
							'class' => 'mini',
							'type' => 'text');
							
		$options[] = array( 'desc' => 'If image left empty -> Category caption 2nd row',
							'id' => $shortname.'_cap2_2',
							'class' => 'mini',
							'type' => 'text');							
							
		$options[] = array( "name" => "Home - Left Col, 3rd Cat",
							"desc" => "Upload an image headline for this block",
							"id" => $shortname.$themeid."_capimg3",
							"type" => "upload");
							
		$options[] = array( 'desc' => 'If image left empty -> Category caption 1st row',
							'id' => $shortname.'_cap3_1',
							'class' => 'mini',
							'type' => 'text');
							
		$options[] = array( 'desc' => 'If image left empty -> Category caption 2nd row',
							'id' => $shortname.'_cap3_2',
							'class' => 'mini',
							'type' => 'text');							
							
		$options[] = array( "name" => "Home - Left Col, 4th Cat",
							"desc" => "Upload an image headline for this block",
							"id" => $shortname.$themeid."_capimgn1",
							"type" => "upload");
							
		$options[] = array( 'desc' => 'If image left empty -> Category caption 1st row',
							'id' => $shortname.'_capn1_1',
							'class' => 'mini',
							'type' => 'text');
							
		$options[] = array( 'desc' => 'If image left empty -> Category caption 2nd row',
							'id' => $shortname.'_capn1_2',
							'class' => 'mini',
							'type' => 'text');							

		$options[] = array( "name" => "Home - Left Col, 5th Cat",
							"desc" => "Upload an image headline for this block",
							"id" => $shortname.$themeid."_capimgn2",
							"type" => "upload");
							
		$options[] = array( 'desc' => 'If image left empty -> Category caption 1st row',
							'id' => $shortname.'_capn2_1',
							'class' => 'mini',
							'type' => 'text');
							
		$options[] = array( 'desc' => 'If image left empty -> Category caption 2nd row',
							'id' => $shortname.'_capn2_2',
							'class' => 'mini',
							'type' => 'text');							
							
		$options[] = array( "name" => "Home - Mid Col, Top left of 4 vertical cols", "desc" => "Upload an image headline for Mid Col, Top left of 4 vertical cols",
							"id" => $shortname.$themeid."_capimg5",
							"type" => "upload");

		$options[] = array( 'desc' => 'If image left empty -> Category caption 1st row',
							'id' => $shortname.'_cap5_1',
							'class' => 'mini',
							'type' => 'text');
							
		$options[] = array( 'desc' => 'If image left empty -> Category caption 2nd row',
							'id' => $shortname.'_cap5_2',
							'class' => 'mini',
							'type' => 'text');							
							
		$options[] = array( "Home - Mid Col, Top right of 4 vertical cols", "desc" => "Upload an image headline for this block",
							"id" => $shortname.$themeid."_capimg6",
							"type" => "upload");								
							
		$options[] = array( 'desc' => 'If image left empty -> Category caption 1st row',
							'id' => $shortname.'_cap6_1',
							'class' => 'mini',
							'type' => 'text');
							
		$options[] = array( 'desc' => 'If image left empty -> Category caption 2nd row',
							'id' => $shortname.'_cap6_2',
							'class' => 'mini',
							'type' => 'text');							
							
		$options[] = array( "name" => "Home -  Mid Col, Bottom left of 4 vertical cols", "desc" => "Upload an image headline for this block",
							"id" => $shortname.$themeid."_capimg7",
							"type" => "upload");		

		$options[] = array( 'desc' => 'If image left empty -> Category caption 1st row',
							'id' => $shortname.'_cap7_1',
							'class' => 'mini',
							'type' => 'text');
							
		$options[] = array( 'desc' => 'If image left empty -> Category caption 2nd row',
							'id' => $shortname.'_cap7_2',
							'class' => 'mini',
							'type' => 'text');							
							
		$options[] = array( "name" => "Home - Mid Col, Bottom right of 4 vertical cols", "desc" => "Upload an image headline for this block",
							"id" => $shortname.$themeid."_capimg8",
							"type" => "upload");	

		$options[] = array( 'desc' => 'If image left empty -> Category caption 1st row',
							'id' => $shortname.'_cap8_1',
							'class' => 'mini',
							'type' => 'text');
							
		$options[] = array( 'desc' => 'If image left empty -> Category caption 2nd row',
							'id' => $shortname.'_cap8_2',
							'class' => 'mini',
							'type' => 'text');							
	
		$options[] = array( "name" => "Home - Right Col, 1st Cat",
							"Upload an image headline for this block",
							"id" => $shortname.$themeid."_capimg10",
							"type" => "upload");		
							
		$options[] = array( 'desc' => 'If image left empty -> Category caption 1st row',
							'id' => $shortname.'_cap10_1',
							'class' => 'mini',
							'type' => 'text');
							
		$options[] = array( 'desc' => 'If image left empty -> Category caption 2nd row',
							'id' => $shortname.'_cap10_2',
							'class' => 'mini',
							'type' => 'text');									
							
		$options[] = array( "name" => "Home - Right Col, 2nd Cat",
							"Upload an image headline for this block",
							"id" => $shortname.$themeid."_capimg11",
							"type" => "upload");		

		$options[] = array( 'desc' => 'If image left empty -> Category caption 1st row',
							'id' => $shortname.'_cap11_1',
							'class' => 'mini',
							'type' => 'text');
							
		$options[] = array( 'desc' => 'If image left empty -> Category caption 2nd row',
							'id' => $shortname.'_cap11_2',
							'class' => 'mini',
							'type' => 'text');		
							
		$options[] = array( "name" => "Home - Right Col, 3rd Cat",
							"Upload an image headline for this block",
							"id" => $shortname.$themeid."_capimg12",
							"type" => "upload");	
							
		$options[] = array( 'desc' => 'If image left empty -> Category caption 1st row',
							'id' => $shortname.'_cap12_1',
							'class' => 'mini',
							'type' => 'text');
							
		$options[] = array( 'desc' => 'If image left empty -> Category caption 2nd row',
							'id' => $shortname.'_cap12_2',
							'class' => 'mini',
							'type' => 'text');		
							
		$options[] = array( "name" => "Home - Right Col, 4th Cat",
							"Upload an image headline for this block",
							"id" => $shortname.$themeid."_capimg13",
							"type" => "upload");
							
		$options[] = array( 'desc' => 'If image left empty -> Category caption 1st row',
							'id' => $shortname.'_cap13_1',
							'class' => 'mini',
							'type' => 'text');
							
		$options[] = array( 'desc' => 'If image left empty -> Category caption 2nd row',
							'id' => $shortname.'_cap13_2',
							'class' => 'mini',
							'type' => 'text');		
							
		$options[] = array( "name" => "Home - Right Col, 5th Cat",
							"Upload an image headline for this block",
							"id" => $shortname.$themeid."_capimg14",
							"type" => "upload");		
							
		$options[] = array( 'desc' => 'If image left empty -> Category caption 1st row',
							'id' => $shortname.'_cap14_1',
							'class' => 'mini',
							'type' => 'text');
							
		$options[] = array( 'desc' => 'If image left empty -> Category caption 2nd row',
							'id' => $shortname.'_cap14_2',
							'class' => 'mini',
							'type' => 'text');		
	$options[] = array( "name" => "Sliders", "type" => "heading");
								
		$options[] = array( 'name' => 'Featured Slider',	
							"desc" => "Slide function",
							"id" => $shortname.$themeid."_sfunc",
							"std" => "fade",
							"type" => "select",
							"options" => $options_feaslide);					
				
		$options[] = array( "desc" => "Auto rotation speed. Slide to next slide in X seconds",
							"id" => $shortname.$themeid."_featimeout",
							"std" => 10,
							"type" => "select",
							"class" => "mini",
							"options" => $options_nr);
							
		$options[] = array( "desc" => "The transition speed (in seconds)",
							"id" => $shortname.$themeid."_feaspeed",
							"std" => 1,
							"type" => "select",
							"class" => "mini",
							"options" => $options_nr);

			$options[] = array( 'name' => 'Inner-Page Slider',
								'desc' => 'Automatically create slideshow of uploaded photos in post entries. Inner-page slider will be displayed below the post title. [Note: Options include displaying slider site-wide, tag-based, or to disable it completely].',
								'id' => $shortname.'_inslider',
								'type' => 'select',
								'class' => 'mini',
								'options' => $options_inslider);
																
			$options[] = array( 'desc' => 'If tag-based option is selected, enter the tag assigned to posts that will display the inner-page slider. <br/> [Note: Only posts with multiple image attachments AND having this specific tag will qualify].',
								'id' => $shortname.'_inslider_tag',
								'class' => 'mini',
								'type' => 'text'); 

	$options[] = array( "name" => "Image Handling", "type" => "heading");

		$options[] = array( "name" => "Catch First Image",
							"desc" => "If enabled, built-in theme functions will scan post from top to bottom, catch the first image, and auto-generate a thumbnail for posts that do not have an image attached or custom field defined.",
							"id" => $shortname.$themeid."_catch_img",
							"std" => 0,
							"type" => "checkbox");
							
		$options[] = array( 'name' => 'TimThumb or WordPress Post Thumbnails',
							'desc' => "Disable TimThumb and use WordPress Post Thumbnails instead [WPMU Mode] [Check only if you are having problems with front page thumbnails].",
							"id" => $shortname."_wpmumode",
							"std" => 0,
							"type" => "checkbox");	

	$options[] = array( "name" => "Footer Options", "type" => "heading");

		$options[] = array( "name" => "Cover Image",
						"desc" => "Upload a cover image for footer left column (leave empty to disable)",
						"id" => $shortname."_footer1img",
						"type" => "upload");
						
		$options[] = array( "desc" => "Add a link for cover image.",
							"id" => $shortname."_footer1link",
							"type" => "text");
	
		$options[] = array( "name" => "Enable Footer Text 1",
							"desc" => "Activate to add the custom text on footer.",
							"id" => $shortname."_footer1",
							"std" => 0,
							"type" => "checkbox"); 

		$options[] = array( "name" => "Custom Text (Left)",
							"desc" => "Custom HTML and Text that will appear in the footer of your theme.",
							"id" => $shortname."_footer1_text",
							"type" => "textarea");
								
		$options[] = array( "name" => "Enable Custom Footer (Right)",
							"desc" => "Activate to add the custom text below to the theme footer.",
							"id" => $shortname."_footer2",
							"std" => 0,
							"type" => "checkbox"); 

		$options[] = array( "name" => "Custom Text (Right)",
							"desc" => "Custom HTML and Text that will appear in the footer of your theme.",
							"id" => $shortname."_footer2_text",
							"type" => "textarea");

	$options[] = array( "name" => "Ad management", "type" => "heading");	
				
				
		$options[] = array( "name" => "Header Ad 728x90",
							"desc" => "728x90 ad code for header above site logo.",
							"id" => $shortname.$themeid."_ad1",
							"type" => "textarea");	
							
		$options[] = array( "name" => "Header Ad 234x90",
							"desc" => "234x90 ad code for header above site logo.",
							"id" => $shortname.$themeid."_ad2",
							"type" => "textarea");				
				
		$options[] = array( "name" => "Header Ad 468x60",
							"desc" => "468x60 ad code for header (will show if header type is set as <strong>Logo - Banner</strong>).",
							"id" => $shortname.$themeid."_ad3",
							"type" => "textarea");	
							
		$options[] = array( "name" => "Top Margin Value",
							"desc" => "Give a margin value 468xp60 ad and top line.",
							"id" => $shortname.$themeid."_adm3",
							"std" => "0",
							"class" => "mini",
							"type" => "text");
							
		$options[] = array( "name" => "Header Ad 234x90 - left",
							"desc" => "234x90 ad code for header - left hand of logo (will show if header type is set as <strong>Ad - Logo - Ad</strong>).",
							"id" => $shortname.$themeid."_ad4",
							"type" => "textarea");

		$options[] = array( "name" => "Header Ad 234x90 - right",
							"desc" => "234x90 ad code for header - right hand of logo (will show if header type is set as <strong>Ad - Logo - Ad</strong>).",
							"id" => $shortname.$themeid."_ad5",
							"type" => "textarea");							
							
		$options[] = array( "name" => "Top Margin Value",
							"desc" => "Give a margin value for both 234x90 ads on right and hand side of logo ads and top line.",
							"id" => $shortname.$themeid."_adm4",
							"std" => "0",
							"class" => "mini",
							"type" => "text");
							
		$options[] = array( "name" => "Homepage Ad 468x60",
							"desc" => "468x60 ad code for mid columd below first block on front page.",
							"id" => $shortname.$themeid."_ad6",
							"type" => "textarea");

		$options[] = array( "name" => "Sidebar 300x250 - top",
							"desc" => "Sidebar ad 300x250 - top.",
							"id" => $shortname.$themeid."_ad7",
							"type" => "textarea");
							
		$options[] = array( "name" => "Sidebar 300x250 - bottom",
							"desc" => "Sidebar ad 300x250 - bottom.",
							"id" => $shortname.$themeid."_ad8",
							"type" => "textarea");
							

	$options[] = array( "name" => "Miscellaneous", "type" => "heading");	

		$options[] = array(	'name' => 'Archive -  Category Template with Slider',
						'desc' => 'ID number of cat(s) to use category template with slider. Separate with comma if more than 1 category is entered. (ex: 1,5,99)',
						'id' => $shortname.$themeid.'_cslide',
						'class' => 'mini',
						'type' => 'text'); 
						
		$options[] = array(	'name' => 'Archive - Magazine Style Category Template',
						'desc' => 'ID number of cat(s) to use category template with slider. Separate with comma if more than 1 category is entered. (ex: 1,5,99)',
						'id' => $shortname.$themeid.'_mag',
						'class' => 'mini',
						'type' => 'text'); 
	
		$options[] = array(	'name' => 'Media category template',
							'desc' => 'ID number of cat(s) to use media gallery template. Separate with comma if more than 1 category is entered. (ex: 1,5,99)',
							'id' => $shortname.$themeid.'_mediatmp',
							'class' => 'mini',
							'type' => 'text'); 	
							
		$options[] = array(	'name' => '2 col category template',
							'desc' => 'ID number of cat(s) to use 2 column category template  Separate with comma if more than 1 category is entered. (ex: 1,5,99)',
							'id' => $shortname.$themeid.'_2col',
							'class' => 'mini',
							'type' => 'text'); 
							
		$options[] = array(	'name' => '4 col category template',
							'desc' => 'ID number of cat(s) to use 4 column category template  Separate with comma if more than 1 category is entered. (ex: 1,5,99)',
							'id' => $shortname.$themeid.'_4col',
							'class' => 'mini',
							'type' => 'text'); 							
	
		$options[] = array(	'name' => 'Media Gallery Title ',
						'desc' => 'Will be displayed at top of Media Gallery page template.',
						'id' => $shortname.$themeid.'_mediahead',
						'class' => 'mini',
						'type' => 'text'); 

		$options[] = array(	'name' => 'Media Gallery Head',
						'desc' => 'Tagline to display below Media Gallery Headline',
						'id' => $shortname.$themeid.'_mediatag',
						'class' => 'mini',
						'type' => 'text'); 
	
		$options[] = array( "name" => "Featured Image Post Display",
							"desc" => "Auto resize and display featured image on single post - above entry.",
							"id" => $shortname."_autoimage",
							"std" => 0,
							"type" => "checkbox");		
	
		$options[] = array( "name" => "Facebook Meta Tags",
							"desc" => "Extract Facebook meta tags on header [Enable only if you are going to use Gabfire Share Widget]",
							"id" => $shortname."_fbonhead",
							"std" => 0,
							"type" => "checkbox");	

		$options[] = array( "name" => "Shortcodes",
							"desc" => "Enable shortcodes functionality",
							"id" => $shortname."_shortcodes",
							"std" => 0,
							"type" => "checkbox");								
							
		$options[] = array( "name" => "Widget Map",
							"desc" => "Display the location of widgets on front page. After checking widget locations <strong>be sure to uncheck this option</strong>",
							"id" => $shortname."_widget",
							"std" => 0,
							"type" => "checkbox");
							
	$options[] = array( "name" => "Custom Styling", "type" => "heading");	
	
		$options[] = array( "name" => "Custom CSS",
							"desc" => "Quickly add custom CSS to your theme by adding it to this block.",
							"id" => $shortname."_custom_css",
							"type" => "textarea");		

		$options[] = array( "name" => "Custom Styles",
							"desc" => "Enable custom styles. Options below will only have affect if this is enabled.",
							"id" => $shortname.$themeid."_customcolors",
							"std" => 0,
							"type" => "checkbox");							

		$options[] = array( "name" => "Body Font Setting",
							"desc" => "Specify the body font properties",
							"id" => $shortname."_body_font",
							"std" => array("size" => "12px","face" => "Arial, Tahoma, sans-serif","style" => "normal","color" => "#555555"),
							"type" => "typography"); 
							
		$options[] = array( "desc" => "Link Color",
							"id" => $shortname."_linkclr",
							"type" => "color");

		$options[] = array( "name" => "PostTitle Font Settings",
							"desc" => "Specify the title font properties",
							"id" => $shortname."_title_font",
							"std" => array("size" => "12px","face" => "Arial, Tahoma, sans-serif","style" => "normal","color" => "#555555"),
							"type" => "typography");
						
		$options[] = array( "desc" => "Post title link color",
							"id" => $shortname."_ptaclr",
							"type" => "color");

		$options[] = array( "desc" => "Post title hovered link color",
							"id" => $shortname."_ptahovclr",
							"type" => "color");				

		$options[] = array( "name" => "Category Name Font Settings",
							"desc" => "Regular, single line category name font settings",
							"id" => $shortname."_catname_font",
							"std" => array("size" => "12px","face" => "Arial, Tahoma, sans-serif","style" => "normal","color" => "#555555"),
							"type" => "typography");
							
		$options[] = array( "desc" => "Link color",
							"id" => $shortname."_catname_ptaclr",
							"type" => "color");

		$options[] = array( "desc" => "Hovered link color",
							"id" => $shortname."_catname_ptahovclr",
							"type" => "color");	
							
		$options[] = array( "name" => "Category Name Font Settings - First Row",
							"desc" => "Specify the title font properties",
							"id" => $shortname."_catname1_font",
							"std" => array("size" => "12px","face" => "Arial, Tahoma, sans-serif","style" => "normal","color" => "#555555"),
							"type" => "typography");							
						
		$options[] = array( "desc" => "Link color",
							"id" => $shortname."_catname1_ptaclr",
							"type" => "color");

		$options[] = array( "desc" => "Hovered link color",
							"id" => $shortname."_catname1_ptahovclr",
							"type" => "color");							
						
		$options[] = array( "name" => "Category Name Font Settings - Second Row",
							"desc" => "Specify the title font properties",
							"id" => $shortname."_catname2_font",
							"std" => array("size" => "12px","face" => "Arial, Tahoma, sans-serif","style" => "normal","color" => "#555555"),
							"type" => "typography");
							
		$options[] = array( "desc" => "Link color",
							"id" => $shortname."_catname2_ptaclr",
							"type" => "color");

		$options[] = array( "desc" => "Hovered link color",
							"id" => $shortname."_catname2_ptahovclr",
							"type" => "color");																		

	update_option("of_template",$options); 			 
	update_option("of_themename",$themename); 
	update_option("of_shortname",$shortname);

	return $options;
}
?>