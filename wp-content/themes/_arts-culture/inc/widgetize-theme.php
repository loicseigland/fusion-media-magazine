<?php
function gabfire_register_sidebar($args) {
	$common = array(
		'before_widget' => '<div id="%1$s" class="widget %2$s"><div class="widgetinner">',
		'after_widget'  => "</div></div>\n",
		'before_title'  => '<h3 class="widgettitle">',
		'after_title'   => "</h3>\n"
	);

	$args = wp_parse_args($args, $common);

	return register_sidebar($args);
}
gabfire_register_sidebar(array('name' => 'Sidebar-1','id' => 'Sidebar-1','description' => 'Sidebar widget below first ad'));
gabfire_register_sidebar(array('name' => 'Sidebar-2','id' => 'Sidebar-2','description' => 'Sidebar widget below second ad'));
gabfire_register_sidebar(array('name' => 'Post-Sidebar-1','id' => 'Post-Sidebar-1','description' => 'Single postpage sidebar widget above first sidebar ad'));
gabfire_register_sidebar(array('name' => 'Post-Sidebar-2','id' => 'Post-Sidebar-2','description' => 'Single postpage sidebar widget below first sidebar ad'));
gabfire_register_sidebar(array('name' => 'Post-Sidebar-Leftcol','id' => 'Post-Sidebar-Leftcol','description' => '3 Column post sidebar - Left block'));
gabfire_register_sidebar(array('name' => 'Page-Sidebar-Leftcol','id' => 'Page-Sidebar-Leftcol','description' => '3 Column page sidebar - Left block'));
gabfire_register_sidebar(array('name' => 'Author-Sidebar-1','id' => 'Author-Sidebar-1','description' => 'Sidebar widget below first ad'));
gabfire_register_sidebar(array('name' => 'Author-Sidebar-2','id' => 'Author-Sidebar-2','description' => 'Sidebar widget below second ad'));
gabfire_register_sidebar(array('name' => 'Magazine-Leftsidebar','id' => 'Magazine-Leftsidebar','description' => 'Magazine style category template - left sidebar'));
gabfire_register_sidebar(array('name' => 'Magazine-Rightsidebar','id' => 'Magazine-Rightsidebar','description' => 'Magazine style category template - right sidebar'));
gabfire_register_sidebar(array('name' => 'LeftCol1','id' => 'LeftCol1','description' => 'Homepage below slider, left column widget zone #1'));
gabfire_register_sidebar(array('name' => 'LeftCol2','id' => 'LeftCol2','description' => 'Homepage below slider, left column widget zone #2'));
gabfire_register_sidebar(array('name' => 'LeftCol3','id' => 'LeftCol3','description' => 'Homepage below slider, left column widget zone #3'));
gabfire_register_sidebar(array('name' => 'LeftCol4','id' => 'LeftCol4','description' => 'Homepage below slider, left column widget zone #4'));
gabfire_register_sidebar(array('name' => 'LeftCol5','id' => 'LeftCol5','description' => 'Homepage below slider, left column widget zone #5'));
gabfire_register_sidebar(array('name' => 'LeftCol6','id' => 'LeftCol6','description' => 'Homepage below slider, left column widget zone #6'));
gabfire_register_sidebar(array('name' => 'MidCol1','id' => 'MidCol1','description' => 'Homepage below slider, mid column widget zone #1'));
gabfire_register_sidebar(array('name' => 'MidCol2','id' => 'MidCol2','description' => 'Homepage below slider, mid column widget zone #2'));
gabfire_register_sidebar(array('name' => 'MidCol3','id' => 'MidCol3','description' => 'Homepage below slider, mid column widget zone #3'));
gabfire_register_sidebar(array('name' => 'MidCol4','id' => 'MidCol4','description' => 'Homepage below slider, mid column widget zone #4'));
gabfire_register_sidebar(array('name' => 'MidCol5','id' => 'MidCol5','description' => 'Homepage below slider, mid column widget zone #5'));
gabfire_register_sidebar(array('name' => 'MidCol6','id' => 'MidCol6','description' => 'Homepage below slider, mid column widget zone #6'));
gabfire_register_sidebar(array('name' => 'MidCol7','id' => 'MidCol7','description' => 'Homepage below slider, mid column widget zone #7'));
gabfire_register_sidebar(array('name' => 'MidCol8','id' => 'MidCol8','description' => 'Homepage below slider, mid column widget zone #8'));
gabfire_register_sidebar(array('name' => 'MidCol9','id' => 'MidCol9','description' => 'Homepage below slider, mid column widget zone #9'));
gabfire_register_sidebar(array('name' => 'MidCol10','id' => 'MidCol10','description' => 'Homepage below slider, mid column widget zone #10'));
gabfire_register_sidebar(array('name' => 'MidCol11','id' => 'MidCol11','description' => 'Homepage below slider, mid column widget zone #11'));
gabfire_register_sidebar(array('name' => 'RightCol1','id' => 'RightCol1','description' => 'Homepage below slider, right column widget zone #1'));
gabfire_register_sidebar(array('name' => 'RightCol2','id' => 'RightCol2','description' => 'Homepage below slider, right column widget zone #2'));
gabfire_register_sidebar(array('name' => 'RightCol3','id' => 'RightCol3','description' => 'Homepage below slider, right column widget zone #3'));
gabfire_register_sidebar(array('name' => 'RightCol4','id' => 'RightCol4','description' => 'Homepage below slider, right column widget zone #4'));
gabfire_register_sidebar(array('name' => 'RightCol5','id' => 'RightCol5','description' => 'Homepage below slider, right column widget zone #5'));
gabfire_register_sidebar(array('name' => 'RightCol6','id' => 'RightCol6','description' => 'Homepage below slider, right column widget zone #6'));
gabfire_register_sidebar(array('name' => 'Footer-1','id' => 'Footer-1','description' => 'Footer 1'));
gabfire_register_sidebar(array('name' => 'Footer-2','id' => 'Footer-2','description' => 'Footer 2'));
gabfire_register_sidebar(array('name' => 'Footer-3','id' => 'Footer-3','description' => 'Footer 3'));
gabfire_register_sidebar(array('name' => 'Footer-4','id' => 'Footer-4','description' => 'Footer 4'));
gabfire_register_sidebar(array('name' => 'Footer-5','id' => 'Footer-5','description' => 'Footer 5'));
gabfire_register_sidebar(array('name' => '2col-belowpost','id' => '2col-belowpost','description' => '2 column single post layout, below entry'));
gabfire_register_sidebar(array('name' => '3col-belowpost','id' => '3col-belowpost','description' => '3 column single post layout, below entry'));
gabfire_register_sidebar(array('name' => 'fullcol-belowpost','id' => 'fullcol-belowpost','description' => 'Full column single post layout, below entry'));
gabfire_register_sidebar(array('name' => 'PageWidget','id' => 'PageWidget','description' => 'Widgetized page template - below entry'));