<?php 
$count = 1;
if (have_posts()) : while (have_posts()) : the_post();			
$gab_thumb = get_post_meta($post->ID, 'thumbnail', true);
$gab_video = get_post_meta($post->ID, 'video', true);
$gab_flv = get_post_meta($post->ID, 'videoflv', true);
$ad_flv = get_post_meta($post->ID, 'adflv', true);
$gab_iframe = get_post_meta($post->ID, 'iframe', true);
 ?>

	<div id="post-<?php the_ID(); ?>" <?php post_class('entry loop-default'); ?>>
		<h2 class="entry_title">
			<a href="<?php the_permalink() ?>" rel="bookmark" title="<?php printf( esc_attr__( 'Permalink to %s', 'artcltr' ), the_title_attribute( 'echo=0' ) ); ?>" ><?php the_title(); ?></a>
		</h2>				
	
		<?php 
		gab_media(array(
			'name' => 'ac-loop-default', 
			'imgtag' => 1,
			'link' => 1,
			'enable_video' => 1, 
			'catch_image' => of_get_option('of_catch_img', 0),
			'video_id' => 'featured', 
			'enable_thumb' => 1, 
			'resize_type' => 'c', /* c to crop, h to resize only height, w to resize only width */
			'media_width' => 630, 
			'media_height' => 350, 
			'thumb_align' => 'aligncenter',
			'enable_default' => 0
		)); 										
		?>
		<p class="top_postmeta">
			<span class="entrydate metaitem"><?php _e('Posted on','artcltr'); echo ' ' . get_the_date(); ?></span>
			<span class="entryby metaitem"><?php _e('By','artcltr'); ?> <a href="<?php echo get_author_posts_url(get_the_author_meta( 'ID' )); ?>"><?php echo get_the_author(); ?></a></span>
			<span class="entrycat metaitem last"><?php echo the_category(', '); echo get_the_term_list( $post->ID, 'gallery-cat', '', ' ', '' ); ?></span>
		</p>						
							
		<?php the_excerpt(); ?>
	</div>

<?php $count++; endwhile; else: endif; ?>