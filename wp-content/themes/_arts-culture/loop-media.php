<div class="post" id="gab_gallery">
	<?php 
	$count = 1;
	if (have_posts()) : while (have_posts()) : the_post();			
	$gab_thumb = get_post_meta($post->ID, 'thumbnail', true);
	$gab_video = get_post_meta($post->ID, 'video', true);
	$gab_flv = get_post_meta($post->ID, 'videoflv', true);
	$ad_flv = get_post_meta($post->ID, 'adflv', true);
	$gab_iframe = get_post_meta($post->ID, 'iframe', true);	
	?>
		<div id="post-<?php the_ID(); ?>" class="media-wrapper <?php if($count % 3 == 0) { echo ' last'; } ?>">
			
			<div class="entry">
				
				<?php 
				if (($gab_flv) or ($gab_video) or ($gab_iframe) ) {
					gab_media(array(
						'name' => 'gabfire',
						'enable_video' => 1,
						'enable_thumb' => 0,
						'media_width' => 310, 
						'media_height' => 230, 
						'thumb_align' => 'alignnone', 
						'enable_default' => 0
					));
				} else { ?>
					<a href="<?php the_permalink(); ?>" title="<?php printf( esc_attr__( 'Permalink to %s', 'artcltr' ), the_title_attribute( 'echo=0' ) ); ?>" rel="bookmark">
							<?php
							gab_media(array(
								'name' => 'ac-medialoop',
								'imgtag' => 1,
								'link' => 0,
								'enable_video' => 0,
								'enable_thumb' => 1,
								'catch_image' => of_get_option('of_catch_img', 0),
								'resize_type' => 'c',
								'media_width' => 310, 
								'media_height' => 230, 
								'thumb_align' => 'alignnone', 
								'enable_default' => 1,
								'default_name' => 'loop-media.jpg'
								));
							?>
					</a>
				<?php } ?>
				
				<h2 class="posttitle s_title">
					<?php
					$gab_iframe = str_replace("http://www.", "http://", $gab_iframe);
					
					$orj_value = array("http://youtube.com/watch?v=", "http://vimeo.com/",   "http://dailymotion.com/video/", "http://screenr.com/" );
					$new_value = array("http://youtube.com/embed/", "http://player.vimeo.com/video/", "http://dailymotion.com/embed/video/", "http://screenr.com/embed/");
					$gab_iframe = str_replace($orj_value, $new_value, $gab_iframe);		
					
					$orj_value_final = array("http://youtube.com", "http://dailymotion.com", "http://screenr.com" );
					$new_value_final = array("http://www.youtube.com", "http://www.dailymotion.com", "http://www.screenr.com");
					$gab_iframe = str_replace($orj_value_final, $new_value_final, $gab_iframe); 
					?>				
				
					<a href="<?php the_permalink(); ?>" title="<?php printf( esc_attr__( 'Permalink to %s', 'artcltr' ), the_title_attribute( 'echo=0' ) ); ?>" rel="bookmark"><?php the_title(); ?></a>
				</h2>
				<p class="small-text">
					<?php _e('Filed under','artcltr'); ?>
					<?php the_category(', '); echo get_the_term_list( $post->ID, 'gallery-cat', ': ', ' ', '' ); ?> 
				</p>					
			</div><!-- .entry -->
			<span class="entry-shadow"></span>
		</div><!-- .media-wrapper -->
			
		<?php if($count % 3 == 0) { echo '<div class="clear"></div>'; } ?>
	<?php $count++; endwhile; endif; ?>
	<div class="clear"></div>	
</div>
<div class="clear"></div>	