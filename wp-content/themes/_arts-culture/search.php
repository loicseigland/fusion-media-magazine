<?php get_header(); ?>

<div id="container" class="wrapper">

			<div id="content">			
					<?php 
					include (TEMPLATEPATH . '/loop-default.php'); 
					
					// load pagination
					if (($wp_query->max_num_pages > 1) && (function_exists('pagination'))) {
						pagination($additional_loop->max_num_pages);
					}
					?>
			</div><!-- #content -->
			
			<div id="sidebar">
				<?php get_sidebar(); ?>
			</div><!-- #Sidebar -->		
			<div class="clear"></div>
	</div><!-- #Container -->

<?php get_footer();