<form class="searchform" action="<?php echo home_url('/'); ?>">
	<fieldset>
		<input type="text" id="s" name="s" value="<?php _e('Search in site...','artcltr'); ?>" onfocus="if(this.value==this.defaultValue)this.value='';" onblur="if(this.value=='')this.value=this.defaultValue;"/>
		<input type="image" class="submitsearch" src="<?php echo get_template_directory_uri(); ?>/framework/images/search.png" alt="<?php _e('Search in site...','artcltr'); ?>" value="" /> 
	</fieldset>
</form>