<div class="sidebarinner">

<?php if(of_get_option('of_ac_tabcontent') == 1) { ?>
	<div id="default_tabs" class="widget">
		
		<ul class="tabs">
			<li><a href="#" class="popularnews"><?php echo of_get_option('of_ac_poplabel'); ?></a></li>
			<li><a href="#" class="recentnews"><?php echo of_get_option('of_ac_reclabel'); ?></a></li>
		</ul>
		<div class="clear"></div>
		<div class="widgetinner">			
			<div class="panes">
				<div>
					<?php
					$count=1;
					$args = array( 'posts_per_page'=> of_get_option('of_ac_popularnr'), 'orderby' => 'comment_count');						
					$gab_query = new WP_Query();$gab_query->query($args); 
					while ($gab_query->have_posts()) : $gab_query->the_post();
					?>
					<div class="featuredpost<?php if($count == of_get_option('of_ac_popularnr')) { echo ' lastpost'; } ?>">
						<?php
						gab_media(array(
							'name' => 'ac-default_ajaxtabs', 
							'imgtag' => 1,
							'link' => 1,
							'enable_video' => 0, 
							'catch_image' => of_get_option('of_catch_img', 0),
							'video_id' => 'default_ajaxtabs', 
							'enable_thumb' => 1,
							'resize_type' => 'c', /* c to crop, h to resize only height, w to resize only width */
							'media_width' => 80, 
							'media_height' =>  51, 
							'thumb_align' => 'alignleft',
							'enable_default' => 0
						)); 
						?>
						<h2 class="posttitle s_title">
							<a href="<?php the_permalink() ?>" rel="bookmark" title="<?php printf( esc_attr__( 'Permalink to %s', 'artcltr' ), the_title_attribute( 'echo=0' ) ); ?>" >
								<?php gab_posttitle(23,'&hellip;'); ?>
							</a>
						</h2>											
						
						<?php echo '<p>' . string_limit_words(get_the_excerpt(), 7) . '&hellip;</p>'; ?>
					</div>
					<?php $count++; endwhile; wp_reset_query(); ?>
				</div>
				
				<div>
					<?php
					$count=1;
					$args = array( 'posts_per_page'=> of_get_option('of_ac_recentnr'));						
					$gab_query = new WP_Query();$gab_query->query($args); 
					while ($gab_query->have_posts()) : $gab_query->the_post();
					?>
					<div class="featuredpost<?php if($count == of_get_option('of_ac_recentnr')) { echo ' lastpost'; } ?>">
						<?php
						gab_media(array(
							'name' => 'default_ajaxtabs', 
							'imgtag' => 1,
							'link' => 1,
							'enable_video' => 1, 
							'catch_image' => of_get_option('of_catch_img', 0),
							'video_id' => 'belowfeamid3', 
							'enable_thumb' => 1,
							'resize_type' => 'c', /* c to crop, h to resize only height, w to resize only width */
							'media_width' => 80, 
							'media_height' =>  51, 
							'thumb_align' => 'alignleft',
							'enable_default' => 0
						)); 
						?>
						<h2 class="posttitle s_title">
							<a href="<?php the_permalink() ?>" rel="bookmark" title="<?php printf( esc_attr__( 'Permalink to %s', 'artcltr' ), the_title_attribute( 'echo=0' ) ); ?>" >
								<?php gab_posttitle(23,'&hellip;'); ?>
							</a>
						</h2>											
						
						<?php echo '<p>' . string_limit_words(get_the_excerpt(), 7) . '&hellip;</p>'; ?>
					</div>
					<?php $count++; endwhile; wp_reset_query(); ?>
				</div>
			</div>
		</div><!-- WidgetInner-->
	</div><!-- default_tabs -->
<?php } ?>

<?php 
	if (is_single()) {
		gab_categoryad('sidebar_300x250-top');
		gab_dynamic_sidebar('Post-Sidebar-1');
	} else {
		gab_categoryad('sidebar_300x250-top');
		gab_dynamic_sidebar('Sidebar-1');
	}
?>

<?php if (intval(of_get_option('of_ac_custompostsnr')) > 0 ) { ?>
<div class="widget">
	<div class="widgetinner">
	<p class="catname">
		<a href="<?php echo get_category_link(of_get_option('of_ac_custompostscat')); ?>"><?php echo get_cat_name(of_get_option('of_ac_custompostscat')); ?></a>
	</p>
	<?php
	$count = 1;
	global $do_not_duplicate, $post; 	
	$args = array(
	  'post__not_in'=>$do_not_duplicate,
	  'posts_per_page' => of_get_option('of_ac_custompostsnr'),
	  'cat' => of_get_option('of_ac_custompostscat')
	);	
	$gab_query = new WP_Query();$gab_query->query($args); 
	while ($gab_query->have_posts()) : $gab_query->the_post();
	if (of_get_option('of_dnd') == 1) { $do_not_duplicate[] = $post->ID; }
	?>
		<div class="featuredpost<?php if($count == of_get_option('of_ac_custompostsnr')) { echo ' lastpost'; } ?>">
			<h2 class="posttitle">
				<a href="<?php the_permalink() ?>" rel="bookmark" title="<?php printf( esc_attr__( 'Permalink to %s', 'source' ), the_title_attribute( 'echo=0' ) ); ?>" ><?php the_title(); ?></a>
			</h2>   
			<?php
			gab_media(array(
				'name' => 'gabfire',
				'imgtag' => 1,
				'link' => 1,
				'enable_video' => 0,
				'video_id' => 'custom-widget',
				'catch_image' => 0,
				'enable_thumb' => 1,
				'resize_type' => 'c',
				'media_width' => of_get_option('of_ac_custompoststwidth'), 
				'media_height' => of_get_option('of_ac_custompoststheight'), 
				'thumb_align' => 'alignleft',
				'enable_default' => 0
			));
			?>
			<p><?php echo string_limit_words(get_the_excerpt(), of_get_option('of_ac_exceprtl')) . '&hellip;</p>'; ?>
		</div><!-- .featuredpost -->
	<?php $count++; endwhile; wp_reset_query(); ?>
	</div>
</div>
<?php } ?>

<?php 
	if (is_single()) {
		gab_categoryad('sidebar_300x250-bottom');
		gab_dynamic_sidebar('Post-Sidebar-2');
	} else {
		gab_categoryad('sidebar_300x250-bottom');	
		gab_dynamic_sidebar('Sidebar-2');
	}
?>
</div><!-- .sidebarinner -->