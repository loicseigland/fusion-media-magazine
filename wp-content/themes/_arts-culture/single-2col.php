<div id="container" class="wrapper">

	<div id="contentwrapper"> 
		<div id="content">

						<div id="post-<?php the_ID(); ?>" <?php post_class('entry'); ?>>
							<h2 class="entry_title">
								<a href="<?php the_permalink() ?>" rel="bookmark" title="<?php printf( esc_attr__( 'Permalink to %s', 'artcltr' ), the_title_attribute( 'echo=0' ) ); ?>" ><?php the_title(); ?></a>
							</h2>	

							<p class="top_postmeta">
								<span class="entrydate metaitem"><?php _e('Posted on','artcltr'); echo ' ' . get_the_date(); ?></span>
								<span class="entryby metaitem"><?php _e('By','artcltr'); ?> <a href="<?php echo get_author_posts_url(get_the_author_meta( 'ID' )); ?>"><?php echo get_the_author(); ?></a></span>
								<span class="entrycat metaitem last"><?php echo the_category(', '); echo get_the_term_list( $post->ID, 'gallery-cat', '', ' ', '' ); ?></span>
							</p>	

							<?php 
							if (of_get_option('of_autoimage') == 1) { 
								$enableimage = 1;
							} else {
								$enableimage = 0;
							}
							
							// Display edit post link to site admin
							edit_post_link(__('Edit This Post','artcltr'),'<p>','</p>'); 					
							
							gab_media(array(
								'name' => 'ac-loop-default', 
								'imgtag' => 1,
								'link' => 0,
								'enable_video' => 1, 
								'catch_image' => of_get_option('of_catch_img', 0),
								'video_id' => 'featured', 
								'enable_thumb' => $enableimage,
								'resize_type' => 'w', /* c to crop, h to resize only height, w to resize only width */
								'media_width' => 630, 
								'media_height' => 400, 
								'thumb_align' => 'aligncenter',
								'enable_default' => 0
							));									
							
							// Display content
							the_content();
							
							// make sure any blognewsed content gets cleared
							echo '<div class="clear"></div>';
						
							the_tags('<p class="posttags">',', ','</p>');
						
							echo '<div class="clear"></div>';
						
							$source_post = get_post_meta($post->ID, 'credit', true);
							if($source_post !== '') {
								echo '<div class="postcredit"><strong>'; _e('Source', 'source'); echo '</strong>: ' .  $source_post . '</div>';
							}						
						
							// Post Widget
							gab_dynamic_sidebar('2col-belowpost');					
							
							// Display pagination
							wp_link_pages('before=<p>&after=</p>&next_or_number=number&pagelink= %');
						
							// Display edit post link to site admin
							edit_post_link(__('Edit This Post','artcltr'),'<p>','</p>'); 
							?>
						</div>
				<?php comments_template(); ?>	
		</div><!-- #content -->
				
		<div id="sidebar">
			<?php get_sidebar(); ?>
		</div><!-- #Sidebar -->		
	</div><!-- #contentwrapper -->		
</div><!-- #Container -->