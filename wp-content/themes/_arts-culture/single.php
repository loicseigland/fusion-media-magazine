<?php 
get_header();

	if (have_posts()) : while (have_posts()) : the_post();
		/* based on user defined tag on theme control panel
		 * and checking the tags/terms assigned for the post
		 * we are going to call single-default.php or 
		 * single-bigpicture.php
		 */
		 
		gab_innerslider();
	
		$post_layout = get_post_meta($post->ID, 'gabfire_post_template', true);		
	
		if ($post_layout == '3col') {
			include (TEMPLATEPATH . '/single-3col.php');
			
		} elseif ($post_layout == 'fullcol') {
			include (TEMPLATEPATH . '/single-fullwidth.php');
			
		} else {
			include (TEMPLATEPATH . '/single-2col.php'); 
		}
		
	endwhile; else : endif;
get_footer();