<?php
/*
	Template Name: Media
*/

get_header();
?>

<div id="container" class="wrapper">

	<div class="categoryhead">
		<h3 class="categoryname"><?php echo of_get_option('of_ac_mediahead'); ?></h3>
		<p><?php echo of_get_option('of_ac_mediatag'); ?></p>
	</div>

	<div id="contentwrapper"> 
		<div id="content" class="fullwidth">
			<?php 
				if ( get_query_var( 'paged') ) $paged = get_query_var( 'paged' ); elseif ( get_query_var( 'page') ) $paged = get_query_var( 'page' ); else $paged = 1;
				query_posts( "post_type=post&showposts=9&paged=$paged" ); 
				include (TEMPLATEPATH . '/loop-media.php'); 
				
				// load pagination if needed
				if (($wp_query->max_num_pages > 1) && (function_exists('pagination'))) {
					pagination($additional_loop->max_num_pages);
				}
				wp_reset_query();
			?>
		</div><!-- #content -->
					
	</div><!-- #contentwrapper -->		
</div><!-- #Container -->

<?php get_footer(); ?>