<?php
/*
	Template Name: Sitemap
*/

get_header();
?>

<div id="container" class="wrapper">

	<div id="contentwrapper"> 
		<div id="content">
			<?php 
			$count = 1;
			if (have_posts()) : while (have_posts()) : the_post();			
			$gab_thumb = get_post_meta($post->ID, 'thumbnail', true);
			$gab_video = get_post_meta($post->ID, 'video', true);
			$gab_flv = get_post_meta($post->ID, 'videoflv', true);
			$ad_flv = get_post_meta($post->ID, 'adflv', true);
			$gab_iframe = get_post_meta($post->ID, 'iframe', true);
			 ?>

				<div id="post-<?php the_ID(); ?>" <?php post_class('entry'); ?>>
					<h1 class="entry_title"><?php the_title(); ?></h1>				
				
					<?php 
					// Display edit post link to site admin
					edit_post_link(__('Edit This Post','artcltr'),'<p>','</p>'); 					
					
					gab_media(array(
						'name' => 'ac-loop-default', 
						'enable_video' => 1, 
						'catch_image' => of_get_option('of_catch_img', 0),
						'video_id' => 'featured', 
						'enable_thumb' => 0, 
						'resize_type' => 'w',
						'media_width' => 630, 
						'media_height' => 300, 
						'thumb_align' => 'aligncenter',
						'enable_default' => 0
					));										
					
					// Display content
					the_content();
					
					// make sure any blognewsed content gets cleared
					echo '<div class="clear"></div>';				
					
					// Display pagination
					wp_link_pages('before=<p>&after=</p>&next_or_number=number&pagelink= %');
				
					// Display edit post link to site admin
					edit_post_link(__('Edit This Post','artcltr'),'<p>','</p>'); 
					?>
				</div>

			<?php $count++; endwhile; else: endif; ?>
			
			<div class="entry">
			
				<div style="width:40%" class="left">  
				<h3><?php _e( 'Pages', 'artcltr' ) ?></h3>
				<ul class="sitemap">
					<?php wp_list_pages( 'depth=0&sort_column=menu_order&title_li=' ); ?>		
				</ul>
				</div>				

				<div style="width:30%" class="left">												  	    
				<h3><?php _e( 'Categories', 'artcltr' ) ?></h3>
				<ul class="sitemap">
					<?php wp_list_categories( 'title_li=&hierarchical=0&show_count=1') ?>	
				</ul>
				</div>
				
				<div style="width:30%" class="left">												  	    
				<h3><?php _e( 'Media Gallery', 'artcltr' ) ?></h3>
				<ul class="sitemap">
					<?php wp_list_categories('taxonomy=gallery-cat&hide_empty=0&title_li='); ?>
				</ul>	
				</div>					
				<div class="clear"></div>
				
				<?php
			   // This is where loop for archives list starts
				$cats = get_categories();
				foreach ($cats as $cat) {
				query_posts('cat='.$cat->cat_ID);
				?>
					<div class="widget">
					<h4><?php echo $cat->cat_name; ?></h4>
					<ul>	
						<?php while (have_posts()) : the_post(); ?>
						<li><a href="<?php the_permalink() ?>"><?php the_title(); ?></a> - (<?php echo $post->comment_count ?>)</li>
						<?php endwhile;  ?>
					</ul>
					</div>
				<?php } ?>
		
				<h3><?php _e( 'Media Gallery Entries', 'artcltr' ); ?></h3>
				<?php
			   // This is where loop for archives list starts
				query_posts('post_type=gab_gallery&showposts=999'); 
				?>
					<div class="widget">
					<ul>	
						<?php while (have_posts()) : the_post(); ?>
						<li><a href="<?php the_permalink() ?>"><?php the_title(); ?></a> - (<?php echo $post->comment_count ?>)</li>
						<?php endwhile;  ?>
					</ul>
					</div>
				<?php
				edit_post_link(__('Edit This Post','artcltr'),'<p>','</p>'); 
				?>			
			</div><!-- Entry -->
			
		</div><!-- #content -->
				
		<div id="sidebar">
			<?php get_sidebar(); ?>
		</div><!-- #Sidebar -->		
	</div><!-- #contentwrapper -->		
</div><!-- #Container -->

<?php get_footer(); ?>