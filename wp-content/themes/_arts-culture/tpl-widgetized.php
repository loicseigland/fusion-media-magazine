<?php
/*
	Template Name: Widgetized
*/

get_header();
?>

<div id="container" class="wrapper">

	<div id="contentwrapper"> 
		<div id="content">
			<?php 
			$count = 1;
			if (have_posts()) : while (have_posts()) : the_post();			
			$gab_thumb = get_post_meta($post->ID, 'thumbnail', true);
			$gab_video = get_post_meta($post->ID, 'video', true);
			$gab_flv = get_post_meta($post->ID, 'videoflv', true);
			$ad_flv = get_post_meta($post->ID, 'adflv', true);
			$gab_iframe = get_post_meta($post->ID, 'iframe', true);
			 ?>

				<div id="post-<?php the_ID(); ?>" <?php post_class('entry'); ?>>
					<h1 class="entry_title"><?php the_title(); ?></h1>				
				
					<?php 
					// Display edit post link to site admin
					edit_post_link(__('Edit This Post','artcltr'),'<p>','</p>'); 					
					
					gab_media(array(
						'name' => 'ac-loop-default', 
						'imgtag' => 1,
						'link' => 1,
						'enable_video' => 1, 
						'catch_image' => of_get_option('of_catch_img', 0),
						'video_id' => 'featured', 
						'enable_thumb' => 0, 
						'resize_type' => 'w', /* c to crop, h to resize only height, w to resize only width */
						'media_width' => 630, 
						'media_height' => 300, 
						'thumb_align' => 'aligncenter',
						'enable_default' => 0
					)); 										
					
					// Display content
					the_content();
					
					// make sure any blognewsed content gets cleared
					echo '<div class="clear"></div>';
					
					// Post Widget
					gab_dynamic_sidebar('PageWidget');					
					
					// Display pagination
					wp_link_pages('before=<p>&after=</p>&next_or_number=number&pagelink= %');
				
					// Display edit post link to site admin
					edit_post_link(__('Edit This Post','artcltr'),'<p>','</p>'); 
					?>
				</div>

			<?php $count++; endwhile; else: endif; ?>		
		</div><!-- #content -->
				
		<div id="sidebar">
			<?php get_sidebar(); ?>
		</div><!-- #Sidebar -->		
	</div><!-- #contentwrapper -->		
</div><!-- #Container -->

<?php get_footer(); ?>