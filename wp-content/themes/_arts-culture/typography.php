<?php
				echo '<style type="text/css" media="screen">';
					echo "body {";
					$body_font = of_get_option('of_body_font'); 
					if ($body_font){
						if ($body_font['size']){ echo "font-size: ".$body_font['size'].";"; }
						if ($body_font['face']){ echo "font-family: ".$body_font['face'].";"; }
						if ($body_font['style']){ echo "font-weight: ".$body_font['style'].";"; }
						if ($body_font['color']){ echo "color: ".$body_font['color'].";"; }
					}
					echo "}";

					$link_color = of_get_option('of_linkclr'); 
					if ($link_color){
						echo "a{";
							if ($link_color){ echo "color: ".$link_color.";"; }
						echo "}";
					}
					
					$posttitle = of_get_option('of_title_font'); 
					if ($posttitle){
						echo "h2.posttitle {";
							if ($posttitle['size']){ echo "font-size: ".$posttitle['size'].";"; }
							if ($posttitle['face']){ echo "font-family: ".$posttitle['face'].";"; }
							if ($posttitle['style']){ echo "font-weight: ".$posttitle['style'].";"; }
							if ($posttitle['color']){ echo "color: ".$posttitle['color'].";"; }
						echo "}";
					}

					$posttitle_color = of_get_option('of_ptaclr'); 
					if ($posttitle_color){
						echo "h2.posttitle a{";
							if ($posttitle_color){ echo "color: ".$posttitle_color.";"; }
						echo "}";
					}

					$posttitle_hover_color = of_get_option('of_ptahovclr'); 
					if ($posttitle_hover_color){
						echo "h2.posttitle a:hover{";
							if ($posttitle_hover_color){ echo "color: ".$posttitle_hover_color.";"; }
						echo "}";
					}
					
					$catname = of_get_option('of_catname_font'); 
					if ($catname){
						echo ".catname {";
							if ($catname['size']){ echo "font-size: ".$catname['size'].";"; }
							if ($catname['face']){ echo "font-family: ".$catname['face'].";"; }
							if ($catname['style']){ echo "font-weight: ".$catname['style'].";"; }
							if ($catname['color']){ echo "color: ".$catname['color'].";"; }
						echo "}";
					}

					$catname_color = of_get_option('of_catname_ptaclr'); 
					if ($catname_color){
						echo ".catname a{";
							if ($catname_color){ echo "color: ".$catname_color.";"; }
						echo "}";
					}

					$catname_hover_color = of_get_option('of_catname_ptahovclr'); 
					if ($catname_hover_color){
						echo ".catname a:hover{";
							if ($catname_hover_color){ echo "color: ".$catname_hover_color.";"; }
						echo "}";
					}
					
					$catname1 = of_get_option('of_catname1_font'); 
					if ($catname1){
						echo ".catname span.firstrow a {";
							if ($catname1['size']){ echo "font-size: ".$catname1['size'].";"; }
							if ($catname1['face']){ echo "font-family: ".$catname1['face'].";"; }
							if ($catname1['style']){ echo "font-weight: ".$catname1['style'].";"; }
							if ($catname1['color']){ echo "color: ".$catname1['color'].";"; }
						echo "}";
					}

					$catname1_color = of_get_option('of_catname1_ptaclr'); 
					if ($catname1_color){
						echo ".catname span.firstrow a {";
							if ($catname1_color){ echo "color: ".$catname1_color.";"; }
						echo "}";
					}

					$catname1_hover_color = of_get_option('of_catname1_ptahovclr'); 
					if ($catname1_hover_color){
						echo ".catname span.firstrow a:hover{";
							if ($catname1_hover_color){ echo "color: ".$catname1_hover_color.";"; }
						echo "}";
					}
					
					$catname2 = of_get_option('of_catname2_font'); 
					if ($catname2){
						echo ".catname span.secondrow a{";
							if ($catname2['size']){ echo "font-size: ".$catname2['size'].";"; }
							if ($catname2['face']){ echo "font-family: ".$catname2['face'].";"; }
							if ($catname2['style']){ echo "font-weight: ".$catname2['style'].";"; }
							if ($catname2['color']){ echo "color: ".$catname2['color'].";"; }
						echo "}";
					}

					$catname2_color = of_get_option('of_catname2_ptaclr'); 
					if ($catname2_color){
						echo ".catname span.secondrow a{";
							if ($catname2_color){ echo "color: ".$catname2_color.";"; }
						echo "}";
					}

					$catname2_hover_color = of_get_option('of_catname2_ptahovclr'); 
					if ($catname2_hover_color){
						echo ".catname span.secondrow a:hover{";
							if ($catname2_hover_color){ echo "color: ".$catname2_hover_color.";"; }
						echo "}";
					}
					
				echo '</style>';
